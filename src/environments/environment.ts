// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBDyW3Ouy_7R9SCB-wm61606FNh7kUPzjk",
    authDomain: "prontopcomanda.firebaseapp.com",
    databaseURL: "https://prontopcomanda.firebaseio.com",
    projectId: "prontopcomanda",
    storageBucket: "prontopcomanda.appspot.com",
    messagingSenderId: "955843099072",
    appId: "1:955843099072:web:95ecdf14d5395b6d65b025"
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
