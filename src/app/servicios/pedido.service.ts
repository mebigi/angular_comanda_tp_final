import { Injectable, Input, Output, EventEmitter } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { map, take, delay, tap } from 'rxjs/operators'
import { Observable, of, Timestamp } from 'rxjs';
import { Usuario } from '../clases/usuario';
import { AngularFirestore } from '@angular/fire/firestore';
import { Producto } from '../clases/producto';
import { Pedido } from '../clases/pedido';
import { AuthService } from './auth.service';
import * as firebase from 'firebase';



import * as randomID from 'randomatic';

@Injectable({
  providedIn: 'root'
})
export class PedidoService {

  porcentaje: number;
  finalizado: boolean;
  productoSelecinado: any;
  isLoggedIn: boolean;
  email: string = "no de";
  sucursal: string = "no def";
  localSelecinado: any;
  public codigo: any = "Error"
  spinner: boolean;
  tiuloPedidos: boolean = false;
  hoy: any;
  hoyFecha: Date;
  public codigoEntregado: string = " ";
  pedidos: any[];
  ultimoPedido: any;



  constructor(
    private storagefoto: AngularFireStorage,
    private fireStore: AngularFirestore,
    private authservice: AuthService
  ) {

    this.authservice.auth.subscribe(Usuario => {
      if (Usuario != undefined) {
        this.email = Usuario.email;
        //buscar tiene codigos de pedido
        this.CodigosClientePedido();

      }
    });
    //this.sucursal = this.authservice.usuarioLoguiado.sucursal;

    // Get the `FieldValue` object
    this.hoy = firebase.firestore.FieldValue.serverTimestamp();



  }


  updateTime() {

    var docRef = this.fireStore.collection('hoy').doc('fecha');
    return docRef.update({
      hoy: firebase.firestore.FieldValue.serverTimestamp()
    })
  }

  tienePedidos(estado: string) {
    let lista: any[] = [];
    let lista2: any[] = [];
    let retorno: boolean;

    this.traerTodos('pedidos')
      .subscribe(pedidos => {
        if (pedidos.length > 0) {
          lista = pedidos.filter(item => item.mozo == this.authservice.usuarioLoguiado && item.estado == estado);
          lista2 = pedidos.filter(item => item.productos.filter(prod => prod.estado == estado).length > 0 && item.estado == estado && this.authservice.usuarioLoguiado.perfil != 'Mozo')
          if (lista.length > 0 || lista2.length > 0) {
            console.log("lista", lista);
            console.log("lista2", lista2);
            this.tiuloPedidos = true;
            return true;
          }
        }
      });
  }

  CodigosClientePedido() {
    return this.traerTodos('pedidos')
      .subscribe(pedidos => {
        if (pedidos.length > 0) {
          this.pedidos = pedidos.filter(item => item.cliente.email == this.email);
        }
        if (pedidos != undefined && pedidos.length > 0) {
          this.ultimoPedido = this.pedidos[0];
        }


      });
  }


  buscarUsuarioPorEmail(codigo: string) {
    let productos = this.fireStore.collection('productos', ref => ref.where('codigo', '==', codigo)).snapshotChanges()
      .pipe(map(actions => actions.map(this.documentToDomainObject)));
    return productos;
  }

  documentToDomainObject = _ => {
    const object = _.payload.doc.data();
    object.id = _.payload.doc.id;
    object.codigo = _.payload.doc.id;
    return object;
  }

  documentToDomainObject2 = _ => {
    const object = _.payload.doc.data();
    return object;
  }

  traerFecha() {
    return this.fireStore.collection('hoy').doc('fecha').get()

  }


  actualizarDatos(id: any, res: any) {
    this.fireStore.collection('productos').doc(id).collection('provedoresAuth').add({ res });
  }

  //var file = campoArchivo.get(0).files[0];
  async subir(pedido: Pedido) {
    this.spinner = true;
    let codigo = randomID('Aa0', 5);
    // Admin SDK only
    this.traerTodos('pedidos').subscribe(res => console.info(res)
    );
    let fecha = new Date().toLocaleString("es-ES", { timeZone: "America/Argentina/Buenos_Aires" });
    return this.fireStore.collection('pedidos').doc(codigo).set(JSON.parse(JSON.stringify(pedido)))
      .then((res) => {
        console.log("Document successfully written!");
        this.codigo = codigo;
        this.spinner = false;
      })
      .catch(function (error) {
        this.spinner = false;
        console.error("Error writing document: ", error);
      });
  }

  async subirFoto(filename: string, file: any, codigo: string) {
    var ref = this.storagefoto.ref('pedidos' + '/' + codigo + '_' + filename).put(file).then((res) => {
      this.storagefoto.ref('pedidos' + '/' + codigo + '_' + filename).getDownloadURL().subscribe((URL) => {
        console.log(URL);
        let ref = this.fireStore.collection('pedidos').doc(codigo);
        return ref.update({
          foto: URL,
        });
      })
    })
  }

  traerTodos(coleccion: string) {
    let usuarios = this.fireStore.collection('/' + coleccion, ref => ref.orderBy('fecha', 'desc')).snapshotChanges()
      .pipe(map(actions => actions.map(this.documentToDomainObject)));
    return usuarios;
  }

  traerTodosVendidos() {
    let usuarios = this.fireStore.collection('/vendidos', ref => ref.orderBy('fecha_listo', 'desc')).snapshotChanges()
      .pipe(map(actions => actions.map(this.documentToDomainObject)));
    return usuarios;
  }



  traerTodosMovimientos(uid: string, coleccion: string) {
    let usuarios = this.fireStore.collection(coleccion).doc(uid).collection('/movimientos', ref => ref.orderBy('fecha', 'desc')).snapshotChanges()
      .pipe(map(actions => actions.map(this.documentToDomainObject)));
    return usuarios;

  }


  buscarUId(uid: string) {
    let ref = this.fireStore.collection('pedidos').doc(uid);
    let getDoc = ref.get();
    console.info('el pedido', getDoc);
    return getDoc;
  }




  verDetalle(producto: any): Observable<boolean> {
    this.productoSelecinado = producto;
    return of(true).pipe(
      delay(1000),
      tap(val => this.isLoggedIn = true)
    );
  }

  verDetalleLocal(producto: any): Observable<boolean> {
    this.localSelecinado = producto;
    console.info(this.localSelecinado);
    return of(true).pipe(
      delay(1000),
      tap(val => this.isLoggedIn = true)
    );
  }

  cambiarEstado(codigo: string, estado: string) {
    let ref = this.fireStore.collection('pedidos').doc(codigo);
    let email = this.email;
    let sucursal = this.sucursal;
    let fecha = new Date();
    return ref.update({
      estado: estado,
    });

  }
  modificar(pedido: Pedido) {
    let ref = this.fireStore.collection('pedidos').doc(pedido.codigo);
    return ref.update(JSON.parse(JSON.stringify(pedido)));
  }

  /*const log = {
    usuario: email,
    fecha: fecha.toLocaleDateString() + " " + fecha.toLocaleTimeString(),
    local: sucursal,
    operacion: 'cambio Stock',
    stock: cantidad
  };

  let movimientos = ref.collection('movimientos').add(log);

  const dataLocal = {
    usuario: email,
    nombre: producto.nombre,
    fecha: fecha.toLocaleDateString() + " " + fecha.toLocaleTimeString(),
    operacion: 'cambio Stock',
    stock: cantidad
  };

  let movimientosLocal = this.addLocalMovimientos(sucursal, dataLocal).then(() => {
  });
  */



  cambiarStock(producto: Producto, cantidad: number) {
    let ref = this.fireStore.collection('productos').doc(producto.codigo);
    let email = this.email;
    let sucursal = this.sucursal;
    let fecha = new Date();
    ref.update({
      cantidad: cantidad,
    });

    const log = {
      usuario: email,
      fecha: fecha.toLocaleDateString() + " " + fecha.toLocaleTimeString(),
      local: sucursal,
      operacion: 'cambio Stock',
      stock: cantidad
    };

    let movimientos = ref.collection('movimientos').add(log);

    const dataLocal = {
      usuario: email,
      nombre: producto.nombre,
      fecha: fecha.toLocaleDateString() + " " + fecha.toLocaleTimeString(),
      operacion: 'cambio Stock',
      stock: cantidad
    };

    let movimientosLocal = this.addLocalMovimientos(sucursal, dataLocal).then(() => {
    });
  }

  prueba() {
    return this.fireStore.collection('locales').doc('boca').collection('movimientos').add({
      name: 'Golden Gate Bridge',
      type: 'bridge'
    });
  }


  addLocalMovimientos(local: string, data: any) {
    return this.fireStore.collection('locales').doc(local).collection('movimientos').add(data);
  }



  addMovimientos(coleccion: string, doc: string, data: any) {
    return this.fireStore.collection(coleccion).doc(doc).collection('movimientos').add(data);
  }


  addProductosVendidos(data: any) {
    return this.fireStore.collection('vendidos').add(data);
  }


  addCodigoEmail(data: any) {
    return this.fireStore.collection('codigos').add(data);
  }




}