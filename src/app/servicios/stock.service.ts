import { Injectable, Input, Output, EventEmitter } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class StockService {



  public showSpinner: boolean = false;
  public porcentaje = 0;
  public finalizado = false;
  cosas: Array<any>;
  filename: string;
 



  constructor(
    private storage: AngularFireStorage,
    private fireStore: AngularFirestore,
    private authservice: AuthService
  ) {
      
   }
/*
  subir(filename:string, bob:any,tipoCosas:string, usuario:string) {     
    var ref  = this.storage.ref(tipoCosas + '/' + usuario + '_' + filename).put(bob);
    ref.percentageChanges().subscribe((porcentaje) => {
      this.porcentaje = Math.round(porcentaje);
      console.log("Porcentaje:" + this.porcentaje)
      if (this.porcentaje == 100) {
        this.finalizado = true;
        setTimeout(() => this.storage.ref(tipoCosas + '/' + usuario + '_' + filename).getDownloadURL().subscribe((URL) => {
          console.log(URL);
          this.fireStore.collection(tipoCosas).add(JSON.parse(JSON.stringify(new Foto(usuario, filename, " ", URL))))
          }), 3000);
      }
    });
  }


  traertodos(tipoCosa:string) {
    let cosas: Array<any>;
    cosas = new Array();
    return this.fireStore.collection('/' + tipoCosa).snapshotChanges();
  }

  publicaFoto(tipoCosa, foto:Foto) {
    let usuario = this.authservice.getCurrentUserMail();
    const userRef: AngularFirestoreDocument<any> = this.fireStore.doc(tipoCosa + `/${foto.uidFoto}`);
     userRef.update({ publico: true });
  }

  recuentoVotos(tipoCosa:string, foto:Foto) {
    this.showLoadingSpinner(foto);
    let voto = false;
    let usuario = this.authservice.getCurrentUserMail();
    console.log(usuario);
  
    foto.votos.forEach(element => {
        if (element == usuario && usuario != undefined ) {
          voto = true;
          this.hideLoadingSpinner(foto);
       
        }
      });
    if (!voto) {
      const userRef: AngularFirestoreDocument<any> = this.fireStore.doc(tipoCosa + `/${foto.uidFoto}`);
      foto.votos.push(usuario);
      userRef.update({votos: foto.votos, total_votos: foto.votos.length}).then(()=> this.hideLoadingSpinner(foto)
      )
      return true;
    } else {
      return false;
    }
  }

  showLoadingSpinner(foto:Foto) {
     return foto.spin = true;
}

hideLoadingSpinner(foto:Foto) {
  return  foto.spin = false;

}*/
}

