import { Injectable, Input, Output, EventEmitter } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';

import { map, take, delay, tap } from 'rxjs/operators'

import { Observable, of } from 'rxjs';
import { Usuario } from '../clases/usuario';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class FotoService {
  porcentaje: number;
  finalizado: boolean;
  productoSelecinado: any;

  usuarioSelecinado: any;
  isLoggedIn: boolean;
  accion: string;


  constructor(
    private storagefoto: AngularFireStorage,
    private fireStore: AngularFirestore,
  ) {

  }

  buscarUsuarioPorEmail(email: string) {
    let usuarios = this.fireStore.collection('usuarios', ref => ref.where('email', '==', email)).snapshotChanges()
      .pipe(map(actions => actions.map(this.documentToDomainObject)));
    return usuarios;
  }


  buscarUsuarioPorSucursal(local: string) {
    let usuarios = this.fireStore.collection('usuarios', ref => ref.where('sucursal', '==', local)).snapshotChanges()
      .pipe(map(actions => actions.map(this.documentToDomainObject)));
    return usuarios;
  }

  documentToDomainObject = _ => {
    const object = _.payload.doc.data();
    object.id = _.payload.doc.id;
    return object;
  }


  actualizarDatos(id: any, res: any) {
    this.fireStore.collection('usuarios').doc(id).collection('provedoresAuth').add({ res });
  }

  //var file = campoArchivo.get(0).files[0];
  async subir(filename: string, file: any, usuario: Usuario) {
    let date = new Date();
    let fecha = date.toLocaleDateString() + " " + date.toLocaleTimeString();
    var ref = this.storagefoto.ref(usuario.perfil + '/' + usuario.email + '_' + filename).put(file);
    ref.percentageChanges().subscribe((porcentaje) => {
      this.porcentaje = Math.round(porcentaje);
      console.log("Porcentaje:" + this.porcentaje)
      if (this.porcentaje == 100) {
        this.finalizado = true;
        setTimeout(() => this.storagefoto.ref(usuario.perfil + '/' + usuario.email + '_' + filename).getDownloadURL().subscribe((URL) => {
          console.log(URL);
          usuario.foto = URL;
          var res = this.fireStore.collection('usuarios').doc('usuario_' + usuario.dni).set(JSON.parse(JSON.stringify(usuario))).then(function (docRef) {
            //=this.fireStore.collection('usuarios').add({ nombre: usuario.nombre, email: usuario.email, foto: URL, perfil: usuario.perfil, sucursal:"Centro" }).then(function (docRef) {


          });

        }), 3000);
      }
    });
  }


  traerTodos() {
    let usuarios = this.fireStore.collection('usuarios').snapshotChanges()
      .pipe(map(actions => actions.map(this.documentToDomainObject)));
    return usuarios;

  }

  traerTodosIngresos() {
    let usuarios = this.fireStore.collection('ingresos', ref => ref.orderBy('fecha', 'desc')).snapshotChanges()
      .pipe(map(actions => actions.map(this.documentToDomainObject)));
    return usuarios;

  }


  buscarUId(uid: string) {
    let ref = this.fireStore.collection('usuarios').doc(uid);
    let getDoc = ref.get();
    return getDoc;
  }

  verDetalle(usuario: any, accion?:string): Observable<boolean> {
    this.usuarioSelecinado = usuario;
    this.accion = accion;
    return of(true).pipe(
      delay(1000),
      tap(val => this.isLoggedIn = true)
    );
  }



  traerTodosMovimientos(uid: string, coleccion: string) {
    let usuarios = this.fireStore.collection(coleccion).doc(uid).collection('/movimientos', ref => ref.orderBy('fecha', 'desc')).snapshotChanges()
      .pipe(map(actions => actions.map(this.documentToDomainObject)));
    return usuarios;

  }

  traerTodosPorId(uid: string, coleccion: string) {
    return this.fireStore.collection(coleccion).doc(uid).collection('/movimientos', ref => ref.orderBy('fecha', 'desc')).snapshotChanges();
  }





  cambiar(uid: string, usuario: Usuario) {
    let ref = this.fireStore.collection('usuarios').doc(uid);
    let fecha = new Date();

    ref.update({
      sucursal: usuario.sucursal,
      perfil: usuario.perfil,
    });

    const log = {
      usuario: usuario.email,
      fecha: fecha.toLocaleDateString() + " " + fecha.toLocaleTimeString(),
      local: usuario.sucursal,
      perfil: usuario.perfil,
      operacion: 'modificacion'
    };
    let movimientos = ref.collection('movimientos').add(log);
  }


  addMovimientos(coleccion: string, doc: string, data: any) {
    return this.fireStore.collection(coleccion).doc(doc).collection('movimientos').add(data);
  }

  addIngresos(coleccion: string, doc: string, data: any) {
    return this.fireStore.collection(coleccion).add(data);
  }

  addOperaciones(coleccion: string, doc: string, data: any) {
    return this.fireStore.collection(coleccion).doc(doc).collection('operaciones').add(data);
  }


  modificarUsuario(usuario: Usuario) {
 
    this.fireStore.collection('usuarios').doc(usuario.id).update(JSON.parse(JSON.stringify(usuario))).then(() => console.log("ok mod"));
  }

  bajaUsuario(usuario: Usuario) {

    const datos = {
      baja: usuario.baja,
      motivo: usuario.motivo
    }
    this.fireStore.collection('usuarios').doc(usuario.id).update(datos).then(() => console.log("ok baja"));
  }


  suspenderUsuario(usuario: Usuario) {

    const datos = {
      suspendido: usuario.suspendido,
    }
    this.fireStore.collection('usuarios').doc(usuario.id).update(datos).then(() => console.log("ok baja"));
  }

  traerUsuariouid(uid: string): any {
    console.log(uid);
    return this.fireStore.collection('usuarios').doc(uid).valueChanges();
  }


  
  async subirFoto(filename: string, file: any, codigo: string) {
    var ref = this.storagefoto.ref('usuarios' + '/' + codigo + '_' + filename).put(file).then((res) => {
      this.storagefoto.ref('usuarios' + '/' + codigo + '_' + filename).getDownloadURL().subscribe((URL) => {
        console.log(URL);
        let ref = this.fireStore.collection('usuarios').doc(codigo);
        return ref.update({
          foto: URL,
        });
      })
    })
  }


}