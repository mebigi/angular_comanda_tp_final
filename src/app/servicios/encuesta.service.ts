import { Injectable } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from './auth.service';
import { Encuesta } from '../clases/encuesta';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EncuestaService {
  spinner: boolean;

  constructor(
    private storagefoto: AngularFireStorage,
    private fireStore: AngularFirestore,
    private authservice: AuthService
  ) {}


   //var file = campoArchivo.get(0).files[0];
   subir(encuesta: Encuesta) {
    this.spinner = true;  
   
    return this.fireStore.collection('encuestas').add(JSON.parse(JSON.stringify(encuesta)))
      .then((res) => {
        console.log("Encuesta successfully written!");
              this.spinner = false;
      })
      .catch(function (error) {
        this.spinner = false;
        console.error("Error writing Encuesta: ", error);
      });
  }

  traerTodos(coleccion: string) {
    let usuarios = this.fireStore.collection('/' + coleccion, ref => ref.orderBy('fecha', 'desc')).snapshotChanges()
      .pipe(map(actions => actions.map(this.documentToDomainObject)));
    return usuarios;
  }

  
  documentToDomainObject = _ => {
    const object = _.payload.doc.data();
    object.id = _.payload.doc.id;
    object.codigo = _.payload.doc.id;
    return object;
  }

}