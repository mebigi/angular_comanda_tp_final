import { Injectable } from '@angular/core';
import { Usuario } from '../clases/usuario';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { auth } from 'firebase';
import swal from 'sweetalert';
import * as firebase from 'firebase';

import { AuthService as social, SocialUser } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";





@Injectable({
  providedIn: 'root'
})
export class AuthService {


  public estaloguiado: any = false;
  public usuarioLoguiado: Usuario;
  usuarios: any[];
  user: any;
  token: any;
  auth: any;
  spinner: boolean;
  loggedIn: boolean;
  userFb: SocialUser;
  conVerificacion: boolean;

  constructor(private router: Router, private socialLogin: social, private angularFireAuth: AngularFireAuth, private db: AngularFirestore, private route: Router) {
    angularFireAuth.authState.subscribe(Usuario => (this.estaloguiado = Usuario));
    this.auth = angularFireAuth.authState;
    this.socialLogin.authState.subscribe((user) => {
      this.userFb = user;
      this.loggedIn = (user != null);
    });


  }


  getCurrentUser() {
    var user = this.angularFireAuth.auth.currentUser;
    var name, email, photoUrl, uid, emailVerified, token;

    if (user != null) {
      name = user.displayName;
      email = user.email;
      photoUrl = user.photoURL;
      emailVerified = user.emailVerified;
      uid = user.uid;
      token = user.getIdToken();
      console.log(token);

      // The user's ID, unique to the Firebase project. Do NOT use
      // this value to authenticate with your backend server, if
      // you have one. Use User.getToken() instead.
    }

  }


  persistirLogin(usuario: Usuario) {
    firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION)
      .then(function () {
        return firebase.auth().signInWithEmailAndPassword(usuario.email, usuario.clave);
      })
      .catch(function (error) {
        // Handle Errors here.
        let errorCode = error.code;
        let errorMessage = error.message;
        console.log(errorMessage);
      });
  }

  verificarEmail(emailVerified: any): boolean {
    if (emailVerified !== true) {
      swal("Error", "Email no verificado, ingrese a su cuenta de email para verificar la cuenta", "error");
      this.SendVerificationMail()
        .then(() => {
          console.log("verifica mail");
          
        });
    } else {
      swal("Verificación exitosa!", "Click para continuar", "success");
      return true;
    }
    return false;
  }


  login(usuario: Usuario): Promise<any> {
    return this.angularFireAuth.auth.signInWithEmailAndPassword(
      usuario.email, usuario.clave);
  }


  // Send email verfificaiton when new user sign up
  SendVerificationMail() {
    return this.angularFireAuth.auth.currentUser.sendEmailVerification();
  }


  getCurrentUserId(): string {
    return this.angularFireAuth.auth.currentUser ? this.angularFireAuth.auth.currentUser.uid : null;
  }

  getCurrentUserMail(): string {
    return this.angularFireAuth.auth.currentUser.email;
  }

  // Sign up with email/password
  async SignUp(usuario: Usuario) {
    return this.angularFireAuth.auth.createUserWithEmailAndPassword(usuario.email, usuario.clave)
      .then((result) => {
        console.log("Se registró con éxito");
        this.route.navigate(['login']);
        console.log(result.user)
      }).catch((error) => {
        console.log(error.message)
      })
  }



  SignOut() {
    this.angularFireAuth.auth.signOut().then(() => {
      this.route.navigate(['login']);
    }).catch(function (error) {
      console.log("An error happened.", error);
    });
  }

  facebookLogin() {

    return firebase.auth().getRedirectResult()
    /*
    
        return new Promise<any>((resolve, reject) => {
          let provider = new firebase.auth.FacebookAuthProvider();
          this.angularFireAuth.auth
            .signInWithPopup(provider)
            .then(res => {
              console.log("llego");
              resolve(res);
            }, err => {
              console.log(err);
              reject(err);
            })
        })
        */

  }

  fbRedirect() {

  }



  signInWithGoogle() {
    return this.socialLogin.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  signInWithFB() {
    return this.socialLogin.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  signOut() {
    return this.socialLogin.signOut();
  }

  tokenFb(user: SocialUser) {
    return this.db.collection('tokenes').add(JSON.parse(JSON.stringify(user)))

  }


}