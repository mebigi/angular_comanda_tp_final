export enum MesaEstados {

    esperando= 'con cliente esperando pedido',
    comiendo= 'con cliente comiendo',
    pagando= 'con cliente pagando',
    cerrada= 'cerrada',
    libre= 'libre',
    ocupada= 'ocupada',
    clausurada='clausurada'

}



