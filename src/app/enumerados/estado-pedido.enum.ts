export enum EstadoPedido {
    iniciado = "iniciado",
    pendiente = "pendiente",
    enpreparacion = "en preparación",
    listoaraservir = "listo para servir",
    servido = "servido",
    finalizado = "finalizado",
    cancelado = "cancelado"
}
