import { Directive, Input, ElementRef, Renderer } from '@angular/core';
import { Pedido } from '../clases/pedido';
import { AuthService } from '../servicios/auth.service';
import { PedidoService } from '../servicios/pedido.service';
import { EstadoPedido } from '../enumerados/estado-pedido.enum';

@Directive({
  selector: '[appEstadoPedido]'
})
export class EstadoPedidoDirective {
  @Input() public appEstadoPedido: Pedido;
  constructor(public authservicio: AuthService, public el: ElementRef,
    public renderer: Renderer, private pedidoservicio: PedidoService) {
      this.ngOnInit();
  }

  ngOnInit() {

    this.estAnim(this.appEstadoPedido.estado, 'active');
    console.log('ID element',this.appEstadoPedido.estado);
   
    }

  
    estAnim(elementId, animClasses) {
      document.getElementById(elementId).classList.add(animClasses);
    }

}
