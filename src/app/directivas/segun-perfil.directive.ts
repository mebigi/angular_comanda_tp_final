
import { AuthService } from '../servicios/auth.service';
import { Directive, ElementRef, Renderer, Input } from '@angular/core';
import { ProductoPedido } from '../clases/producto-pedido';
import { EstadoPedido } from '../enumerados/estado-pedido.enum';
import { Pedido } from '../clases/pedido';
import { PedidoService } from '../servicios/pedido.service';
@Directive({
  selector: '[appSegunPerfil]'

})
export class SegunPerfilDirective {

  @Input() public appSegunPerfil: { pedido: Pedido, estado: EstadoPedido };
  constructor(public authservicio: AuthService, public el: ElementRef, public renderer: Renderer, private pedidoservicio: PedidoService) {


  }

  ngOnInit() {

    //si hay algun producto para el estado solicitado
    if (this.filtroPedidos(this.appSegunPerfil.pedido,this.appSegunPerfil.estado) == false) {
      // con esto me aseguro de mostrar todo en admn siempre que coincida el estado de algun prodctu del pedido
      this.renderer.setElementStyle(this.el.nativeElement, 'display', 'none');
    }
    else {
      //si NO hay algun producto, oculto todo
      console.log('No hay productos en este pedido con este estado', this.appSegunPerfil);
    
    }


  }

  filtroPedidos(pedido: any, estado: string): boolean {
    //si hay algun producto para el estado solicitado
   
      if (pedido.productos.some(producto => producto.estado == estado)) {
        // con esto me aseguro de mostrar todo en admn siempre que coincida el estado de algun prodctu del pedido
        if (this.authservicio.usuarioLoguiado.perfil != 'Admin' && this.authservicio.usuarioLoguiado.perfil != undefined) {
          console.log("this.segunEmpleado(estado, pedido):", this.segunEmpleado(estado, pedido), 'estado:', estado)
          return this.segunEmpleado(estado, pedido);
        }
        else if(this.authservicio.usuarioLoguiado.perfil == 'Admin') {
          return true;
        }
      }
  
        //si NO hay algun producto, oculto todo
        return false;
      

  }
 

  segunEmpleado(estado: any, pedido: Pedido): boolean {
    switch (estado) {
      case EstadoPedido.iniciado:
        //me importa que solo lo vea los mozos
        if (this.authservicio.usuarioLoguiado.perfil != 'Mozo') {
          console.log('hide Iniciado');
          return false;
        }
        break;
      case EstadoPedido.pendiente:
        //me importa si es mozo _> que vea solo sus pedidos
      
        if (this.authservicio.usuarioLoguiado.perfil == 'Mozo' && pedido.mozo.email != this.authservicio.usuarioLoguiado.email) {
          console.log('hide pendiente no es el mozo del pedido');
          return false;
        }
        //si es si es otro que los vea
        if (this.authservicio.usuarioLoguiado.perfil != 'Mozo' && this.tipoEmpleadoEstado(pedido.productos, estado) == false) {
          console.log('hide pendiente tiene productos para este empleado');
          return false;
        }

        break;
      case EstadoPedido.enpreparacion:
        //enconces me importa que si es mozo _> que vea solo sus pedidos
        if (this.authservicio.usuarioLoguiado.perfil == 'Mozo' && pedido.mozo.email != this.authservicio.usuarioLoguiado.email) {
          console.log('hide preparando no es el mozo del pedido');
          return false;
        }

        if (this.authservicio.usuarioLoguiado.perfil != 'Mozo' && this.tieneProductosEstado(pedido.productos, estado) == false) {
          console.log('hide preparando tiene productos para este empleado');
          return false;
        }
        break;

      //enconces me importa que si es otro _> que vea solo sus pedidos
      case EstadoPedido.listoaraservir:
        if (this.authservicio.usuarioLoguiado.perfil == 'Mozo' && pedido.mozo.email != this.authservicio.usuarioLoguiado.email) {
          console.log('hide listo no es el mozo del pedido');
          return false;
        }

        if (this.authservicio.usuarioLoguiado.perfil != 'Mozo' && this.tieneProductosEstado(pedido.productos, estado) == false) {
          console.log('hide listo tiene productos para este empleado');
          return false;
        }
        break;
      //idem anteriro
      case EstadoPedido.servido:
        //me importa si es mozo _> que vea solo sus pedidos
        if (this.authservicio.usuarioLoguiado.perfil == 'Mozo' && pedido.mozo.email != this.authservicio.usuarioLoguiado.email) {
          console.log('hide servido no es el mozo del pedido');
          return false;
        }
        //si es si es otro que los vea
        if (this.authservicio.usuarioLoguiado.perfil != 'Mozo') {
          console.log('hide servido tiene productos para este empleado');
          return false;
        }
        break;
      case EstadoPedido.finalizado:
        //me importa si es mozo _> que vea solo sus pedidos
        if (this.authservicio.usuarioLoguiado.perfil == 'Mozo' && pedido.mozo.email != this.authservicio.usuarioLoguiado.email) {
          console.log('hide servido no es el mozo del pedido');
          return false;
        }
        //si es si es otro que los vea
        if (this.authservicio.usuarioLoguiado.perfil != 'Mozo') {
          console.log('hide servido tiene productos para este empleado');
          return false;
        }

        break;
      default:
        break;

    }
    return true;
  }


  tipoEmpleadoEstado(list: any[], estado: any): boolean {

    return list.some(producto => producto.estado == estado && producto.tipo_empleado == this.authservicio.usuarioLoguiado.perfil)

  }

  tieneProductosEstado(list: any[], estado: any) {
    return  list.some(item => (item.empleado != undefined &&(item.empleado.email == this.authservicio.usuarioLoguiado.email) && item.estado == estado));
   
  }

}
