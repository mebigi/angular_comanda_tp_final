import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './componente/login/login.component';
import { RegistroComponent } from './componente/registro/registro.component';
import { ProductoComponent } from './componente/producto/producto.component';
import { ProductosListadoComponent } from './componente/productos-listado/productos-listado.component';
import { ProductoInfoComponent } from './componente/producto-info/producto-info.component';
import { UsuariosListadoComponent } from './componente/usuarios-listado/usuarios-listado.component';
import { UsuarioInfoComponent } from './componente/usuario-info/usuario-info.component';
import { PrincipalComponent } from './componente/principal/principal.component';
import { LocalInfoComponent } from './componente/local-info/local-info.component';
import { LocalesListadoComponent } from './componente/locales-listado/locales-listado.component';
import { ClienteAbmComponent } from './componente/cliente-abm/cliente-abm.component';
import { PedidosEstadosComponent } from './componente/pedidos-estados/pedidos-estados.component';
import { MesaAbmComponent } from './componente/mesa-abm/mesa-abm.component';
import { MesaListaComponent } from './componente/mesa-lista/mesa-lista.component';
import { PedidoAbmComponent } from './componente/pedido-abm/pedido-abm.component';
import { MesasEstadosComponent } from './componente/mesas-estados/mesas-estados.component';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import { EncuestaComponent } from './componente/encuesta/encuesta.component';
import { EncuestaResultadosComponent } from './componente/encuesta-resultados/encuesta-resultados.component';
import { EstadisticasComponent } from './componente/estadisticas/estadisticas.component';
import { IngresosComponent } from './componente/ingresos/ingresos.component';
import { VentasComponent } from './componente/ventas/ventas.component';
import { UsuarioAbmComponent } from './componente/usuario-abm/usuario-abm.component';
import { UsuarioModificarComponent } from './componente/usuario-modificar/usuario-modificar.component';
import { ProductoModificarComponent } from './componente/producto-modificar/producto-modificar.component';
import { MesasEstadisticaComponent } from './componente/mesas-estadistica/mesas-estadistica.component';

const routes: Routes = [

  { path: '', redirectTo: 'inicio', pathMatch: 'full' },
  { path: 'inicio', component: LoginComponent },
  { path: 'inicio:codigo',  component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'Registro', component: RegistroComponent },
  { path: 'Producto', component: ProductoComponent },
  { path: 'Listado-Productos', component: ProductosListadoComponent },
  { path: 'Producto-info', component: ProductoInfoComponent },
  { path: 'Listado-Usuarios', component: UsuariosListadoComponent },
  { path: 'Usuario-info', component: UsuarioInfoComponent },
  { path: 'Local-info', component: LocalInfoComponent },
  { path: 'Locales', component: LocalesListadoComponent },
  { path: 'Principal', component: PrincipalComponent },
  { path: 'Registro/cliente', component: ClienteAbmComponent },
  { path: 'Pedidos-Estados', component: PedidosEstadosComponent },
  { path: 'ABM-Mesa', component: MesaAbmComponent },
  { path: 'ABM-Pedido', component: PedidoAbmComponent },
  { path: 'Lista-Mesas', component: MesaListaComponent },
  { path: 'Mesas-Estados', component: MesasEstadosComponent },
  { path: 'Encuesta', component: EncuestaResultadosComponent },
  { path: 'Estadisticas', component: EstadisticasComponent },
  { path: 'Estadisticas/Ingresos', component: IngresosComponent },
  { path: 'Estadisticas/Ventas', component: VentasComponent },
  { path: 'Estadisticas/Mesas', component: MesasEstadisticaComponent },
  { path: 'ABM-Usuario', component: UsuarioAbmComponent },
  { path: 'Usuario-mod', component: UsuarioModificarComponent },
  { path: 'Producto-mod', component: ProductoModificarComponent },
 

  
  
];




@NgModule({
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
  imports: [
    RouterModule.forRoot(routes, { useHash: true }),

  ],
  exports: [RouterModule],
 
})
export class AppRoutingModule { }
