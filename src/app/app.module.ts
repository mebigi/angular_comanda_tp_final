import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireModule} from '@angular/fire';
import { AngularFireAuthModule} from '@angular/fire/auth';
import { CommonModule } from '@angular/common';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './componente/login/login.component';
import { RegistroComponent } from './componente/registro/registro.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PrincipalComponent } from './componente/principal/principal.component';
import { ProductoComponent } from './componente/producto/producto.component';
import { ProductoInfoComponent } from './componente/producto-info/producto-info.component';
import { ProductosListadoComponent } from './componente/productos-listado/productos-listado.component';
import { UsuarioInfoComponent } from './componente/usuario-info/usuario-info.component';
import { UsuariosListadoComponent } from './componente/usuarios-listado/usuarios-listado.component';
import { TableFilterPipe } from './table-filter.pipe';
import { LocalesListadoComponent } from './componente/locales-listado/locales-listado.component';
import { LocalInfoComponent } from './componente/local-info/local-info.component';
import { ClienteAbmComponent } from './componente/cliente-abm/cliente-abm.component';
import {NgxPrintModule} from 'ngx-print';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { PedidosEstadosComponent } from './componente/pedidos-estados/pedidos-estados.component';
import { MenuComponent } from './componente/menu/menu.component';
import { PerfilPipe } from './pipes/perfil.pipe';
import { SegunPerfilDirective } from './directivas/segun-perfil.directive';
import { CategoriasPipe } from './pipes/categorias.pipe';
import { EstadoPipe } from './pipes/estado.pipe';
import { DateFormatPipe } from './pipes/date-format.pipe';
import { DiaFormatPipe } from './pipes/dia-format.pipe';
import { MesaAbmComponent } from './componente/mesa-abm/mesa-abm.component';
import { MesaListaComponent } from './componente/mesa-lista/mesa-lista.component';
import { PedidoAbmComponent } from './componente/pedido-abm/pedido-abm.component';
import { ScrollComponent } from './componente/scroll/scroll.component';
import { NgSlimScrollModule, SLIMSCROLL_DEFAULTS } from 'ngx-slimscroll';
import { EstadoPedidoClienteComponent } from './componente/estado-pedido-cliente/estado-pedido-cliente.component';
import { EstadoPedidoDirective } from './directivas/estado-pedido.directive';
import { ModificarPedidoComponent } from './componente/modificar-pedido/modificar-pedido.component';
import { MesasEstadosComponent } from './componente/mesas-estados/mesas-estados.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { EncuestaComponent } from './componente/encuesta/encuesta.component';
import { RatingModule } from 'ng-starrating';
import { RankingDirective } from './directivas/ranking.directive';
import { EncuestaResultadosComponent } from './componente/encuesta-resultados/encuesta-resultados.component';
import { EstadisticasComponent } from './componente/estadisticas/estadisticas.component';
import { IngresosComponent } from './componente/ingresos/ingresos.component';
import { RangoFechaPipe } from './pipes/rango-fecha.pipe';
import { OperacionesComponent } from './componente/operaciones/operaciones.component';
import { VentasComponent } from './componente/ventas/ventas.component';
import { CantidadEmpleadoPipe } from './pipes/cantidad-empleado.pipe';
import { RecaptchaModule } from 'ng-recaptcha';
import { ProductoCantidadPipe } from './pipes/producto-cantidad.pipe';
import { SocialLoginModule, AuthServiceConfig } from "angularx-social-login";
import { GoogleLoginProvider, FacebookLoginProvider } from "angularx-social-login";
import { UsuarioAbmComponent } from './componente/usuario-abm/usuario-abm.component';
import { UsuarioModificarComponent } from './componente/usuario-modificar/usuario-modificar.component';

import { ProductoModificarComponent } from './componente/producto-modificar/producto-modificar.component';
import { MesasEstadisticaComponent } from './componente/mesas-estadistica/mesas-estadistica.component';
import { ChartsModule } from 'ng2-charts';

let config = new AuthServiceConfig([
   {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider("658100728294147")
  }
]);


export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistroComponent,
    PrincipalComponent,
    ProductoComponent,
    ProductoInfoComponent,
    ProductosListadoComponent,
    UsuarioInfoComponent,
    UsuariosListadoComponent,
    TableFilterPipe,
    LocalesListadoComponent,
    LocalInfoComponent,
    ClienteAbmComponent,
    PedidosEstadosComponent,
    MenuComponent,
    PerfilPipe,
    SegunPerfilDirective,
    CategoriasPipe,
    EstadoPipe,
    DateFormatPipe,
    DiaFormatPipe,
    MesaAbmComponent,
    MesaListaComponent,
    PedidoAbmComponent,
    ScrollComponent,
    EstadoPedidoClienteComponent,
    EstadoPedidoDirective,
    ModificarPedidoComponent,
    MesasEstadosComponent,
    EncuestaComponent,
    RankingDirective,
    EncuestaResultadosComponent,
    EstadisticasComponent,
    IngresosComponent,
    RangoFechaPipe,
    OperacionesComponent,
    VentasComponent,
    CantidadEmpleadoPipe,
    ProductoCantidadPipe,
    UsuarioAbmComponent,
    UsuarioModificarComponent, 
    ProductoModificarComponent, MesasEstadisticaComponent,
    
 

  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),  
    AngularFireAuthModule,
    AngularFirestoreModule,
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDropzoneModule,
    BrowserModule, 
    RouterModule,
    NgxPrintModule,
    SweetAlert2Module.forRoot(),
    NgSlimScrollModule,
    RatingModule,
    RecaptchaModule,
    SocialLoginModule,
    ChartsModule,


   
  ],
  providers: [ AngularFireStorage, {provide: LocationStrategy, useClass: HashLocationStrategy},
    { provide: AuthServiceConfig,  useFactory: provideConfig}],
  bootstrap: [AppComponent],
  
})
export class AppModule { }
