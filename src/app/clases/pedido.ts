import { Producto } from './producto';
import { Usuario } from './usuario';
import { EstadoPedido } from '../enumerados/estado-pedido.enum';
import { ProductoPedido } from './producto-pedido';

export class Pedido {
    codigo: string;
    cliente: Usuario; //clase cliente
    mesas: any[] = []; // array de mesas    
    productos:ProductoPedido[]; // array de porductos cada prodcuto con su cantidad precio
    importe_total:number;
    mozo: Usuario; // empledo tipo mozo
    fecha:Date;
    estado: EstadoPedido;// (enum)    
    foto:string;
    tiempo_estimado:number = 0;  
    fechaTomado:Date;
    fechaListo:Date;
    encuesta:boolean=false;

    constructor(uncliente?: Usuario){
        
        if(uncliente != undefined){
            this.cliente=uncliente;
        } else{
            this.cliente= new Usuario();
        }
        this.foto= "https://firebasestorage.googleapis.com/v0/b/prontopcomanda.appspot.com/o/pedidos%2Ffood-tray-svg.jpg?alt=media&token=2ec8eded-7bc3-4516-819c-4e4ab8b0036e";
 
    }
}
