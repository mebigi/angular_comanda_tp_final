import { EstadoPedido } from '../enumerados/estado-pedido.enum';

export class Producto {
    codigo: string;
    nombre:string;
    costo:number;
    descripcion:string;
    URL:string;
    tipo_empleado:string;
    categoria:string;
    cantidad:number;
    monto:number;
    estado:EstadoPedido;
    baja: boolean;
    motivo:string = " ";
    constructor(){
        this.baja= false;
     
           }
}

