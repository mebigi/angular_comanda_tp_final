import { Usuario } from './usuario';
import { EstadoPedido } from '../enumerados/estado-pedido.enum';

export class ProductoPedido {
    codigo: string;
    nombre:string;
    costo:number;
    cantidad: number;
    fecha:Date;
    descripcion: string;
    observaciones:string;
    URL:string;
    tipo_empleado:string;
    categoria:string;
    monto:number;
    empleado: Usuario; //el empleado que lo tomo
    estado:EstadoPedido = EstadoPedido.pendiente;

    constructor(){
     }
    
    }
    
