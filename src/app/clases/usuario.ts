export class Usuario 
{
	public id: string;
	public nombre: string;
	public apellido: string;
    public email: string;
	public clave:string;
	public perfil:string;
	public foto:string;
	public estado:string
	public sucursal:string;
	suspendido:boolean;
	baja:boolean;
	dni: string;
	motivo:string;


	constructor(email?: string, clave?:string)
	{
		this.email = email;
		this.clave = clave;
		this.foto ='https://firebasestorage.googleapis.com/v0/b/prontopcomanda.appspot.com/o/pedidos%2FMf5)S_personas.jpg?alt=media&token=82e67dae-0ff0-4371-8187-4545f6f59aa7'
		this.suspendido = false;
		this.baja = false;
	}
}