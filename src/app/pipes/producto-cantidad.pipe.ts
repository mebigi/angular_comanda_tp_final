import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'productoCantidad'
})
export class ProductoCantidadPipe implements PipeTransform {

  transform(list: any[], filters: Object, producto:any) {
    const keys = Object.keys(filters).filter(key => filters[key]);
    const filterUser = user => new Date(user[keys[0]]).getTime() <= new Date(filters[keys[1]]).getTime() && new Date(user[keys[0]]).getTime() >= new Date(filters[keys[0]]).getTime();
  console.log("list.filter(filterUser)", list.filter(filterUser))
    return  this.cantidad(producto, list.filter(filterUser));
   
  }

cantidad(producto: any, ventas:any[]): any {      
  let cantidad =0;     
    ventas.forEach(item => {       
        if (producto == item.producto) {   
        cantidad = cantidad + item.cantidad;
      }  
              
    }
  );

  console.log(producto, ' ', cantidad);
  return cantidad;
}

}
