import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'estado'
})
export class EstadoPipe implements PipeTransform {
    transform(list: any[], estado: string) {   
      if(list != undefined){
        return list.filter(item => item.estado == estado);
        console.log('lista no vacia');
      }

      console.log('lista vacia');
      return false;
    }

}
