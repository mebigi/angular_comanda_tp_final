import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cantidadEmpleado'
})
export class CantidadEmpleadoPipe implements PipeTransform {
 
    transform(list: any[], filters: Object, empleado:any) {
      const keys = Object.keys(filters).filter(key => filters[key]);
      const filterUser = user => new Date(user[keys[0]]).getTime() <= new Date(filters[keys[1]]).getTime() && new Date(user[keys[0]]).getTime() >= new Date(filters[keys[0]]).getTime();
      return  this.cantidad(empleado, list.filter(filterUser));
     
    }

  cantidad(empleado: any, ventas:any[]): any {      
      let cantidad =0;     
      ventas.forEach(item => {        
        if (empleado == item.empleado) {   
          cantidad++;
        }   
       else if (empleado == item.mozo.email) {   
          cantidad++;
        }          
      }
    );
    return cantidad;
  }

}
