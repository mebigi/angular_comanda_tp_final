import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'perfil'
})
export class PerfilPipe implements PipeTransform {


  transform(list: any[], perfil: string) {
    
        return list.filter(item => item.tipo_empleado == perfil);
  }


}
