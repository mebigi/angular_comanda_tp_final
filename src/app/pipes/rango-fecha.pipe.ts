import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'rangoFecha'
})
export class RangoFechaPipe implements PipeTransform {

  transform(list: any[], filters: Object) {
    const keys = Object.keys(filters).filter(key => filters[key]);

    const filterUser = user => new Date(user[keys[0]]).getTime() <= new Date(filters[keys[1]]).getTime() && new Date(user[keys[0]]).getTime() >= new Date(filters[keys[0]]).getTime();
   
    return list.filter(filterUser);
  }

}
