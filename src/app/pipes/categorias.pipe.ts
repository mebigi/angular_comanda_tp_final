import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'categorias'
})
export class CategoriasPipe implements PipeTransform {

 

  transform(list: any[], perfil: string) {

    if(perfil != 'todas'){

      return list.filter(item => item.categoria == perfil);
    }
    else{
      return list.filter(item => item.categoria == 'Entrada')
      .concat(list.filter(item => item.categoria == 'Plato'))
      .concat(list.filter(item => item.categoria == 'Postre'))
      .concat(list.filter(item => item.categoria == 'Bebida'));
      

    }
    
  
}
}
