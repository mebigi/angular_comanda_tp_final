import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MesaService } from 'src/app/servicios/mesa.service';
import { Mesa } from 'src/app/clases/mesa';
import { MesaEstados } from 'src/app/enumerados/mesa-estados.enum';


@Component({
  selector: 'app-mesa-lista',
  templateUrl: './mesa-lista.component.html',
  styleUrls: ['./mesa-lista.component.css']
})
export class MesaListaComponent implements OnInit {
  @Output() seAgrego: EventEmitter<any>;
  @Output() seQuito: EventEmitter<any>;

  mesas: Mesa[] = [];
 
  constructor(public mesaServicio: MesaService) { 
    this.seAgrego = new EventEmitter();
    this.seQuito = new EventEmitter();
  }

  ngOnInit() {
    
    this.traerTodasMesas();
    /*  this.mesaServicio.traerUnaMesa('mesa_3').subscribe(mesa => {
        console.log(mesa.numero);
     });
     */
   // this.traerMesa(3);
  }


  traerTodasMesas(){
  this.mesaServicio.traerTodasMesas()
  .subscribe(mesas => {
    this.mesas = mesas;
    // console.log(this.mesas[0].numero);
  });
}

tomar(mesa: Mesa){
 mesa.habilitada = false;
 mesa.estado = MesaEstados.ocupada;
 this.seAgrego.emit(mesa);
}

liberar(mesa: Mesa){
  mesa.habilitada = true;
  mesa.estado = MesaEstados.libre;
  this.seQuito.emit(mesa);
}

}
