import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MesaListaComponent } from './mesa-lista.component';

describe('MesaListaComponent', () => {
  let component: MesaListaComponent;
  let fixture: ComponentFixture<MesaListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MesaListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MesaListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
