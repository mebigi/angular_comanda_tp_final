import { Component, OnInit } from '@angular/core';
import { ProductoService } from 'src/app/servicios/producto.service';
import { Router } from '@angular/router';
import { FotoService } from 'src/app/servicios/foto.service';

@Component({
  selector: 'app-locales-listado',
  templateUrl: './locales-listado.component.html',
  styleUrls: ['./locales-listado.component.css']
})
export class LocalesListadoComponent implements OnInit {
  locales: any[] = [];

  constructor(private serviciousuarios: FotoService, private servicioproductos: ProductoService, private route:Router) { }

  ngOnInit() {
    this.traerTodos();
    console.info(this.locales);
   // this.prueba();
  }

  traerTodos() {
   this.servicioproductos.traerTodos('locales')
      .subscribe(productos => {
        this.locales = productos;
        // console.log(this.usuarios[0].numero);
      });
    

  }



  mostrar(producto: any){
       //this.paisesService.verDetalle(pais).subscribe(resp => resp);
    this.servicioproductos.verDetalleLocal(producto).subscribe(resp => this.route.navigate(['Local-info']));
  }

}

