import { Component, OnInit } from '@angular/core';
import { FotoService } from 'src/app/servicios/foto.service';
import { DatePipe } from '@angular/common';
import { PedidoService } from 'src/app/servicios/pedido.service';
import * as jsonexport from "jsonexport/dist"
import { saveAs } from 'file-saver';
import { RangoFechaPipe } from 'src/app/pipes/rango-fecha.pipe';

@Component({
  selector: 'app-operaciones',
  templateUrl: './operaciones.component.html',
  styleUrls: ['./operaciones.component.css']
})
export class OperacionesComponent implements OnInit {

  ventas: any[] = [];
  Fechavi: any[] = [];
  Fechavf: any[] = [];
  ventasMozos: any[] = [];
  ventasBartenders: any[] = [];
  ventasCocineros: any[] = [];
  bartenders: any[]  = [];
  cocineros: any[]  = [];
  filtro: any;
  arCsv: any[] = [];

  bartenderCantidad: { dia: string, empleado: string, cantidad: number }[] = [];
  cocineroCantidad: { dia: string, empleado: string, cantidad: number }[] = [];
  mozos: any[] = [];
  mozosVentas: any[] = [];
  ventasFulano: any[] = [];
  fulano: string;

  constructor(private pedidoServicio: PedidoService) { }

  ngOnInit() {

    let pipe2: DatePipe = new DatePipe('en-US');
    this.pedidoServicio.traerTodosVendidos().subscribe(ventas => {
      this.ventas = ventas; console.log(this.ventas);

      this.ventas.forEach(venta => { venta['dia'] = pipe2.transform(venta.fecha_listo, 'MM/dd/yyyy'); venta['diaf'] = pipe2.transform(venta.fecha_listo, 'MM/dd/yyyy'); })
      this.Fechavi = this.getUnique(this.ventas, 'dia');
      this.Fechavf = this.getUnique(this.ventas, 'dia');

      this.ventasBartenders = this.ventas.filter(venta => venta.sector == 'Bartender');
      this.ventasCocineros = this.ventas.filter(venta => venta.sector == 'Cocinero');

      this.bartenders = this.getUnique(this.ventasBartenders, 'empleado').map(
        function (venta) {
          var b = {};
          b['dia'] = pipe2.transform(venta.fecha_listo, 'MM/dd/yyyy');
          b['empleado'] = venta.empleado;
          b['cantidad'] = 0;
          return b;
        });

      this.mozosVentas = this.ventas.map(
        function (venta) {
          var b = {};
          b['dia'] = pipe2.transform(venta.fecha_listo, 'MM/dd/yyyy');
          b['empleado'] = venta.mozo.email;
          b['cantidad'] = 0;
          return b;
        });

      this.mozos = this.getUnique(this.mozosVentas, 'empleado');






      this.cocineros = this.getUnique(this.ventasCocineros, 'empleado').map(function (venta) {
        var b = {};
        b['dia'] = pipe2.transform(venta.fecha_listo, 'MM/dd/yyyy');
        b['empleado'] = venta.empleado;
        b['cantidad'] = 0;
        return b;
      });




    });

  }


  getUnique(arr, comp) {
    var unique = arr
      .map(function (e) { return e[comp]; })
      // store the keys of the unique objects
      .map(function (e, i, final) { return final.indexOf(e) === i && i; })
      // eliminate the dead keys & store unique objects
      .filter(function (e) { return arr[e]; }).map(function (e) { return arr[e]; });
    return unique;
  }


  exportCSV(myData: any[], dato:any) {
    //let myData= JSON.stringify(Data);   

    let filtado= new RangoFechaPipe();
   // filtado.transform(this.ventas, dato);

    jsonexport(filtado.transform(this.ventas, dato), function (err, csv) {
      if (err) return console.log(err);
      console.log(csv);

      let file = new Blob([csv], { type: 'text/csv;charset=utf-8' });
      saveAs(file, 'operaciones.csv')

    });
  }

  cargarOperacionesEmpleado(email:string){
    this.fulano = email;
    this.ventasFulano = this.ventas.filter(venta => venta.empleado == email);

  }




}
