import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/servicios/auth.service';
import { Usuario } from 'src/app/clases/usuario';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { FotoService } from 'src/app/servicios/foto.service';
import { ProductoService } from 'src/app/servicios/producto.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import swal from 'sweetalert';
import { Producto } from 'src/app/clases/producto';

@Component({
  selector: 'app-producto-modificar',
  templateUrl: './producto-modificar.component.html',
  styleUrls: ['./producto-modificar.component.css']
})
export class ProductoModificarComponent implements OnInit {
  public nuevoProducto: Producto = new Producto();
  public archivo: File;
  files: File[] = [];

  perfil: string;
  locales: any[];
  modificacion: boolean;


  public Archivofoto: any;
  ok: boolean = true;
  parse: any;
  productos: Producto[];
  mensaje: string = " ";
  image: any;
  mifoto: any;
  boton: HTMLElement;
  menu_b: boolean = true;
  alta: boolean = true;
  baja: boolean;
  prueba: boolean;
  public formAlta: FormGroup;
  formBaja: FormGroup;
  formMod: FormGroup;
  amodificar:any
  formSus: FormGroup;
  accion: string;
  usuario: Usuario = new Usuario();


  constructor(public formBuilder: FormBuilder, public formBuilder2: FormBuilder, public formBuilder3: FormBuilder, public productoServicio: ProductoService, private servicioproductos: ProductoService, private authservice: AuthService, public fotoservice: FotoService) {
    this.usuario = this.authservice.usuarioLoguiado;
    this.perfil = this.authservice.usuarioLoguiado.perfil;
    this.nuevoProducto = this.productoServicio.productoSelecinado;
    this.accion = this.productoServicio.accion;

  }

  ngOnInit() {
    this.cargarform();
    this.traerTodasproductos();
   
 //this.productoServicio.traerUsuariouid('usuario_13969584').subscribe((res)=> {this.amodificar = res;  
  // console.log(this.amodificar);});
   
  }


  async  createFile(url:string){
    let response = await fetch(url);
    let data = await response.blob();
    let metadata = {
      type: 'image/jpeg'
    };
    return new File([data], "foto.jpg", metadata);
    // ... do something with the file or return it
  }


  onSelect($event) {
    console.log($event);
    this.files.push(...$event.addedFiles);
    this.archivo = this.files[0];
  }

  onRemove($event) {
    console.log($event);
    this.files.splice(this.files.indexOf($event), 1);
  }


  cargarform() {
    this.formBaja = this.formBuilder2.group(
      {
        nombreBaja: ['', Validators.compose([Validators.required])],
        motivoBaja: ['', Validators.compose([Validators.minLength(6), Validators.required])]
      });
    this.formMod = this.formBuilder3.group(
      {
        nombre: ['', Validators.compose([Validators.maxLength(30), Validators.minLength(2), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
        costo: ['', Validators.compose([Validators.required])],
        categoria: ['', Validators.compose([Validators.maxLength(15), Validators.minLength(3), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      descripcion: ['', Validators.compose([Validators.maxLength(66), Validators.minLength(3), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
    
      });
  }

  public getError(form: any, controlName: string): string {
    let error: any;
    let mse = "";
    const control = form.get(controlName);
    if (control.touched && control.errors != null) {
      console.info(JSON.stringify(control.errors));
      error = JSON.parse(JSON.stringify(control.errors));
      if (error.required) {
        mse = "Campo requerido.";
      }
      if (error.minlength != undefined) {
        mse = "Error en logintud mínima requerida.";
      }
      if (error.maxlength != undefined) {
        mse = "Error en la longitud máxima.";
      }

      if (error.pattern != undefined) {
        mse = "Error en el tipo de dato.";
      }
    }
    return mse;
  }


  traerTodasproductos() {
    this.productoServicio.traerTodos('productos')
      .subscribe(productos => {
        this.productos = productos;
        // console.log(this.productos[0].numero);
      });
  }



  bajaProducto() {
    this.mensaje = " ";
    this.estAnim('baja', 'animation-target');
    if (this.productos.some(u => u.codigo == this.nuevoProducto.codigo)) {
      this.nuevoProducto.baja = true;
      this.productoServicio.bajaProducto(this.nuevoProducto);

      this.mensaje = ("Se dió de baja la producto nro " + this.nuevoProducto.codigo);
      swal("Baja exitosa!", this.mensaje, "success");
      this.traerTodasproductos();
      this.cargarform();
    } else {
      swal("Error", "Usuario no encontrado", "error");

    }
    this.nuevoProducto = new Producto();
  }


  estAnim(elementId, animClasses) {
    document.getElementById(elementId).classList.add(animClasses);
    var wait = window.setTimeout(function () {
      document.getElementById(elementId).classList.remove(animClasses)
    },
      1300
    );
  }

  modificarProducto() {
    this.mensaje = " ";

    console.log("entro");
    
    if (this.productos.some(p => p.codigo == this.nuevoProducto.codigo)) {       
      
      this.productoServicio.modificarProducto(this.nuevoProducto);
      this.mensaje = ("Se modificó la usuario nro " + this.nuevoProducto.codigo);
      if (this.archivo != undefined) {
        this.productoServicio.subirFoto(this.archivo.name, this.archivo, this.nuevoProducto.codigo)
      }
      swal("Modificación exitosa!", this.mensaje, "success");
      this.traerTodasproductos();
           } else {
      this.mensaje = ("No se encontró le usuarie indicade");
      swal("Error", this.mensaje, "error");
    }  

  }


  menu(accion: string) {
    switch (accion) {
      case 'alta':
        this.accion = 'alta';      
        this.mensaje = " ";
        break;
      case 'baja':
        this.accion = 'baja';
        this.mensaje = " ";
        break;
      case 'modificar':
        this.accion = 'modificar';
        this.mensaje = " ";
        break;
    }
  }



}


