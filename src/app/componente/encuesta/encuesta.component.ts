import { Component, OnInit, Input } from '@angular/core';
import { StarRatingComponent } from 'ng-starrating';
import { Encuesta } from 'src/app/clases/encuesta';
import { EncuestaService } from 'src/app/servicios/encuesta.service';
import swal2 from 'sweetalert2';
import { PedidoService } from 'src/app/servicios/pedido.service';
import { Pedido } from 'src/app/clases/pedido';
import { AuthService } from 'src/app/servicios/auth.service';
import { MesaService } from 'src/app/servicios/mesa.service';

@Component({
  selector: 'app-encuesta',
  templateUrl: './encuesta.component.html',
  styleUrls: ['./encuesta.component.css']
})
export class EncuestaComponent implements OnInit {

  @Input() pedido: Pedido;
  valor: Encuesta = new Encuesta();
  showEncuesta: boolean = true;
  ahora: Date;


  constructor(private mesasServicio: MesaService, public authservivio: AuthService, private EncuestaServicio: EncuestaService, public pedidoServicio: PedidoService) {
    //this.authservivio.usuarioLoguiado.perfil

  }

  ngOnInit() {

  }



  onRate($event: { oldValue: number, newValue: number, starRating: StarRatingComponent }, tipo: string) {

    switch (tipo) {
      case 'restaurante':
        this.valor.restaurante = $event.newValue;
        break;
      case 'mozo':
        this.valor.mozo = $event.newValue;
        break;
      case 'mesa':
        this.valor.mesa = $event.newValue;
        break;
      case 'bartender':
        this.valor.bartender = $event.newValue;
        break;
      case 'cocinero':
        this.valor.cocinero = $event.newValue;
        break;
      case 'PrecioCalidad':
        this.valor.PrecioCalidad = $event.newValue;

        break;
      default:
        break;
    }
    $event.newValue = 0;
  }


  enviar() {
    this.valor.codigo_pedido = this.pedido.codigo;    
          this.valor.fecha = this.pedido.fecha;
        
          this.EncuestaServicio.subir(this.valor).then(() => {
            swal2.fire({
              title: 'Envío Exitoso!',
              text: 'Se envió la encuesta, gracias por participar',
              icon: 'success',
              confirmButtonText: 'ok'
            });

          });   
       
    this.pedido.mesas.forEach(mesa => {
      const data = {
        numero: mesa.numero,
        id_mesa: mesa.id,
        puntaje: this.valor.mesa,
        codigo_pedido: this.valor.codigo_pedido
      };
      this.mesasServicio.addComentarios('mesas', mesa.id, data);
    })

    this.pedido.encuesta = true;
    this.pedidoServicio.modificar(this.pedido);
    this.valor = new Encuesta();
    this.showEncuesta = false;
  }

}


 

