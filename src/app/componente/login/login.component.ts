import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/servicios/auth.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import swal from 'sweetalert';
import { Usuario } from 'src/app/clases/usuario';
import { FotoService } from 'src/app/servicios/foto.service';
import { Routes, ActivatedRoute, Router, ParamMap } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PedidoService } from 'src/app/servicios/pedido.service';
import { DatePipe } from '@angular/common';
import { SocialUser } from 'angularx-social-login';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  spinner = false;
  usuario: Usuario = new Usuario();

  private ok: boolean; //Login OK
  private error: boolean; //Login fallido
  public formulario: FormGroup;
  private errorDatos: boolean; //Error en el formato de datos de correo o clave
  private enEspera: boolean; //Muestra u oculta el spinner
  public codigo: string;
  public formulario2: FormGroup;
  ahora: Date;
  fechastring: string;

  public user: SocialUser = new SocialUser();
  public loggedIn: boolean;
  conVerificacion: boolean;

  // constructor(public authservicio: AuthService,  public fotoservice: FotoService, private router: Router, private route: ActivatedRoute, public formBuilder: FormBuilder) { }
  constructor(private activatedRoute: ActivatedRoute, private router: Router, public formBuilder: FormBuilder, public authservicio: AuthService, public fotoservice: FotoService, private pedidoServicio: PedidoService) {

    this.activatedRoute.queryParams.subscribe(params => {
      if (params['codigo'] != undefined) {
        this.codigo = params['codigo'];
        console.log('parame', params['codigo'])
        if (this.codigo != '') {
          console.log('parametro', this.codigo, params['codigo'])
          this.enviarCodigo(this.codigo);
        }
      }
      // Print the parameter to the console. 
    });

    /*
            this.activatedRoute.paramMap.subscribe(
              (params: ParamMap) => {
                this.codigo = params.get('codigo');
                console.log('map',params.get('codigo'));
                if( this.codigo != null){
                  
                  this.enviarCodigo(this.codigo);
                }    
              }
            )
       
       
        this.usuario.email="usuario@nose.com";
        /*if(localStorage.getItem('codigo') != undefined){
          this.codigo = localStorage.getItem('codigo');
          console.log(this.codigo)
        }*/

    this.formulario = this.formBuilder.group(
      {
        correo: ['', Validators.compose([Validators.email, Validators.required])],
        clave: ['', Validators.compose([Validators.minLength(6), Validators.required])]
      });

    this.formulario2 = this.formBuilder.group(
      {
        codigo: ['', Validators.compose([Validators.minLength(5), Validators.required])]
      });

    this.loading();

  }

  ngOnInit() {

    //this.authservicio.facebookLogin
    //para purebas luego borrar
    //this.usuario.email = "mebigi@hotmail.com";
    //this.usuario.clave = "123456";
    if (this.authservicio.usuarioLoguiado != undefined) {
      this.authservicio.usuarioLoguiado.email = this.usuario.email;

    }
    //const urlParams = new URLSearchParams(window.location.search);

    // console.log("result codigo",this.getParameterByName('codigo'));


  }

  getParameterByName(name) {
    let url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }



  loading() {
    this.ok = false;
    this.error = false;
    this.errorDatos = false;
    this.enEspera = false;

    setTimeout(() => {    //<<<---    using ()=> syntax
      // this.spinner = false;
    }, 3000);

  }

  public getError(form: any, controlName: string): string {
    let error: any;
    let mse = "";
    const control = form.get(controlName);
    if (control.touched && control.errors != null) {
      console.info(JSON.stringify(control.errors));
      error = JSON.parse(JSON.stringify(control.errors));
      if (error.required) {
        mse = "Campo requerido.";
      }
      if (error.minlength != undefined) {
        mse = "Error en logintud mínima requerida.";
      }
      if (error.maxlength != undefined) {
        mse = "Error en la longitud máxima.";
      }

      if (error.pattern != undefined) {
        mse = "Error en el tipo de dato.";
      }
    }
    return mse;
  }


  /* login2() {
 
      if (this.formulario.valid) {
 
       this.usuario.email = this.formulario.value.correo;
       this.usuario.clave = this.formulario.value.clave;
 
       this.spinner = true;
       this.authservicio.usuarioLoguiado = new Usuario();
       this.authservicio.login(this.usuario).then(() => {
         setTimeout(() => {
           this.usuarioServicio.buscarUsuarioPorEmail(this.usuario.email).subscribe(usuarios => {
             this.authservicio.usuarioLoguiado = usuarios[0];
             this.spinner = false;
             //console.log(this.authservicio.usuarioLoguiado.perfil);
             if (this.authservicio.usuarioLoguiado.perfil == undefined) {
               swal("Error", "Usuario no encontrado", "error");
             } else {
               this.router.navigate(['/principal']);
             }
 
           }, (err) => {
             swal("Error", "Usuario no encontrado", "error");
             this.spinner = false;
             console.log("usuario ni encontrado en base", err);
            
            });
         }, 1500);
       }, (err) => {
         swal("Error", "Usuario no encontrado en Auth", "error");
         this.spinner = false;
         console.log("usuario ni encontrado", err);
        
        });
 
      }
 
   }
 
 */





  public getOk(): boolean {
    return this.ok;
  }

  public getErrorDatos(): boolean {
    return this.errorDatos;
  }

  public getEnEspera(): boolean {
    return this.enEspera;
  }




  facebookLogin() {

    this.authservicio.signInWithFB().then((result) => {
      this.user = result;
      console.info(this.user.email);
      //this.usuario.email = user.email;   
      this.authservicio.tokenFb(result);
      this.spinner = true;

      this.pedidoServicio.updateTime().then(res => {
        this.pedidoServicio.traerFecha()
          .subscribe((doc) => {
            this.ahora = new Date(doc.data().hoy.toDate());

            this.fotoservice.buscarUsuarioPorEmail(this.user.email).subscribe(usuarios => {
              if (usuarios[0] != undefined) {
                this.authservicio.usuarioLoguiado = usuarios[0];
                console.log(" this.authservicio.usuarioLoguiado", this.authservicio.usuarioLoguiado)
                this.spinner = false;
                const data = {
                  usuario: usuarios[0].email,
                  nombre: usuarios[0].nombre,
                  perfil: usuarios[0].perfil,
                  fecha: this.ahora.toString(),
                };
                this.fotoservice.addMovimientos('usuarios', this.authservicio.usuarioLoguiado.id, data);
                this.fotoservice.addIngresos('ingresos', this.authservicio.usuarioLoguiado.id, data);
              }

              if (this.authservicio.usuarioLoguiado.perfil == "") {
                swal("Error", "Usuario y/o clave inválidos", "error");
                this.spinner = false;
              }
              else if (usuarios[0].suspendido) {
                swal("Suspedido", "Usuario se encunatra suspendido", "error");
                this.router.navigate(['/inicio']);
              }
              else {
                this.router.navigate(['/Principal']);
              }

            }, (err) => {

              swal("Error", "Usuario no encontrado", "error");
              this.spinner = false;
              console.log("usuario ni encontrado en base", err);

            });



          }
          )
      })



    }).catch(function (error) {
      // Handle Errors here.
      console.info(error);
      var errorCode = error.code;
      var errorMessage = error.message;
      // The email of the user's account used.
      var email = error.email;
      // The firebase.auth.AuthCredential type that was used.
      var credential = error.credential;
      // ...
    });


  }





  login() {
    this.spinner = true;
    this.usuario.email = this.formulario.value.correo;
    this.usuario.clave = this.formulario.value.clave;

    this.pedidoServicio.updateTime().then(res => {
      this.pedidoServicio.traerFecha()
        .subscribe((doc) => {
          this.ahora = new Date(doc.data().hoy.toDate());
          let f: DatePipe = new DatePipe('en-US');
          this.fechastring = f.transform(new Date(this.ahora), 'dd/MM/yyyy HH:mm:ss');
          console.log("this.fechastring", this.fechastring)
          this.autenticacion();
        }
        )
    });
  }


  loginConVerificacion() {
    this.spinner = true;
    this.usuario.email = this.formulario.value.correo;
    this.usuario.clave = this.formulario.value.clave;

    this.pedidoServicio.updateTime().then(res => {
      this.pedidoServicio.traerFecha()
        .subscribe((doc) => {
          this.ahora = new Date(doc.data().hoy.toDate());
          let f: DatePipe = new DatePipe('en-US');
          this.fechastring = f.transform(new Date(this.ahora), 'dd/MM/yyyy HH:mm:ss');
          console.log("this.fechastring", this.fechastring)
          this.autenticacion();
        }
        )
    });
  }

  enviarCodigo(codigo: string) {
    // localStorage.setItem('codigo', codigo);
    this.spinner = true;
    this.usuario.email = "anonimo@nose.com";
    this.usuario.clave = "123456";
    this.pedidoServicio.buscarUId(codigo).subscribe((res) => {
      if (res.exists) {
        this.pedidoServicio.codigoEntregado = codigo;
        console.log(" this.pedidoServicio.codigoEntregado:", this.pedidoServicio.codigoEntregado, ' codigoIngresado ', codigo)
        this.pedidoServicio.updateTime().then(res => {
          this.pedidoServicio.traerFecha()
            .subscribe((doc) => {
              this.ahora = new Date(doc.data().hoy.toDate());
              let f: DatePipe = new DatePipe('en-US');
              this.fechastring = f.transform(new Date(this.ahora), 'dd/MM/yyyy HH:mm:ss')
              this.autenticacion();
            }
            )
        })
      } else {
        swal("Error", "Código no encontrado", "error");
        this.spinner = false;
      }
    }
    )
    

  }

  autenticacion() {

    this.authservicio.usuarioLoguiado = new Usuario();
    this.authservicio.login(this.usuario).then((result) => {
      if (this.conVerificacion) {
        if (this.authservicio.verificarEmail(result.user.emailVerified)) {
          this.fotoservice.buscarUsuarioPorEmail(this.usuario.email).subscribe(usuarios => {
            if (usuarios[0] != undefined) {
              this.authservicio.usuarioLoguiado = usuarios[0];
              console.log(" this.authservicio.usuarioLoguiado", this.authservicio.usuarioLoguiado)
              this.spinner = false;
              const data = {
                usuario: usuarios[0].email,
                nombre: usuarios[0].nombre,
                perfil: usuarios[0].perfil,
                fecha: this.ahora.toString(),
              };
              this.fotoservice.addMovimientos('usuarios', this.authservicio.usuarioLoguiado.id, data);
              this.fotoservice.addIngresos('ingresos', this.authservicio.usuarioLoguiado.id, data);
            }

            if (this.authservicio.usuarioLoguiado.perfil == "") {
              swal("Error", "Usuario y/o clave inválidos", "error");
              this.spinner = false;
            }
            else if (usuarios[0].suspendido) {
              swal("Suspedido", "Usuario se encuentra suspendido, contacte al Admin", "error");
              this.router.navigate(['/inicio']);
            }
            else if (usuarios[0].baja) {
              swal("Suspedido", "Usuario fue dado de baja", "error");
              this.router.navigate(['/inicio']);
            }
            else {
              this.router.navigate(['/Principal']);
            }

          }, (err) => {
            swal("Error", "Usuario no encontrado", "error");
            this.spinner = false;
            console.log("usuario ni encontrado en base", err);
          });

        } else {
          this.spinner = false;
        }
      }
      else {

        this.fotoservice.buscarUsuarioPorEmail(this.usuario.email).subscribe(usuarios => {
          if (usuarios[0] != undefined) {
            this.authservicio.usuarioLoguiado = usuarios[0];
            console.log(" this.authservicio.usuarioLoguiado", this.authservicio.usuarioLoguiado)
            this.spinner = false;
            const data = {
              usuario: usuarios[0].email,
              nombre: usuarios[0].nombre,
              perfil: usuarios[0].perfil,
              fecha: this.ahora.toString(),
            };
            this.fotoservice.addMovimientos('usuarios', this.authservicio.usuarioLoguiado.id, data);
            this.fotoservice.addIngresos('ingresos', this.authservicio.usuarioLoguiado.id, data);
          }

          if (this.authservicio.usuarioLoguiado.perfil == "") {
            swal("Error", "Usuario y/o clave inválidos", "error");
            this.spinner = false;
          }
          else if (usuarios[0].suspendido) {
            swal("Suspedido", "Usuario se encuentra suspendido, contacte al Admin", "error");
            this.router.navigate(['/inicio']);
          }
          else if (usuarios[0].baja) {
            swal("Suspedido", "Usuario fue dado de baja", "error");
            this.router.navigate(['/inicio']);
          }
          else {
            this.router.navigate(['/Principal']);
          }

        }, (err) => {
          swal("Error", "Usuario no encontrado", "error");
          this.spinner = false;
          console.log("usuario ni encontrado en base", err);
        });

      }
        
    }), (err) => {

    swal("Error", "Usuario no encontrado en Auth", "error");
    this.spinner = false;
    console.log("usuario ni encontrado", err);
  }
  }



autenticacion2() {

  this.authservicio.usuarioLoguiado = new Usuario();

  this.authservicio.login(this.usuario).then(() => {

    this.fotoservice.buscarUsuarioPorEmail(this.usuario.email).subscribe(usuarios => {
      if (usuarios[0] != undefined) {
        this.authservicio.usuarioLoguiado = usuarios[0];
        console.log(" this.authservicio.usuarioLoguiado", this.authservicio.usuarioLoguiado)
        this.spinner = false;
        const data = {
          usuario: usuarios[0].email,
          nombre: usuarios[0].nombre,
          perfil: usuarios[0].perfil,
          fecha: this.ahora.toString(),
        };
        this.fotoservice.addMovimientos('usuarios', this.authservicio.usuarioLoguiado.id, data);
        this.fotoservice.addIngresos('ingresos', this.authservicio.usuarioLoguiado.id, data);
      }

      if (this.authservicio.usuarioLoguiado.perfil == "") {
        swal("Error", "Usuario y/o clave inválidos", "error");
        this.spinner = false;
      }
      else if (usuarios[0].suspendido) {
        swal("Suspedido", "Usuario se encuentra suspendido, contacte al Admin", "error");
        this.router.navigate(['/inicio']);
      }
      else if (usuarios[0].baja) {
        swal("Suspedido", "Usuario fue dado de baja", "error");
        this.router.navigate(['/inicio']);
      }
      else {
        // this.autenticacion();
        this.router.navigate(['/Registro/cliente']);
      }

    }, (err) => {

      swal("Error", "Usuario no encontrado", "error");
      this.spinner = false;
      console.log("usuario ni encontrado en base", err);

    });

  }, (err) => {

    swal("Error", "Usuario no encontrado en Auth", "error");
    this.spinner = false;
    console.log("usuario ni encontrado", err);

  });

}


mensajeRegistro() {


  let soy = {
    buttons: ["Soy cliente", "Soy empleado"],
  }
  swal("Todavía no estás registrado?", soy)
    .then((value) => {

      console.info(value);
      switch (value) {
        case null:
          this.spinner = true;
          this.usuario.email = 'anonimo@nose.com';
          this.usuario.clave = '123456';

          this.pedidoServicio.updateTime().then(res => {
            this.pedidoServicio.traerFecha()
              .subscribe((doc) => {
                this.ahora = new Date(doc.data().hoy.toDate());
                let f: DatePipe = new DatePipe('en-US');
                this.fechastring = f.transform(new Date(this.ahora), 'dd/MM/yyyy HH:mm:ss');
                console.log("this.fechastring", this.fechastring)
                this.autenticacion2();
              }
              )
          })

          break;
        case true:
          swal("No tienes permisos para ingresar, contactarse con Administrador");
          break;

      }
    });

}
}
