import { Component, OnInit } from '@angular/core';
import { Mesa } from 'src/app/clases/mesa';
import { MesaEstados } from 'src/app/enumerados/mesa-estados.enum';
import { MesaService } from 'src/app/servicios/mesa.service';

@Component({
  selector: 'app-mesas-estados',
  templateUrl: './mesas-estados.component.html',
  styleUrls: ['./mesas-estados.component.css']
})
export class MesasEstadosComponent implements OnInit {
  
  mesas: any;
  seAgrego: any;
  seQuito: any;

  constructor(private mesaServicio: MesaService) { }


  
  ngOnInit() {
    
    this.traerTodasMesas();
    /*  this.mesaServicio.traerUnaMesa('mesa_3').subscribe(mesa => {
        console.log(mesa.numero);
     });
     */
   // this.traerMesa(3);
  }


  traerTodasMesas(){
  this.mesaServicio.traerTodasMesas()
  .subscribe(mesas => {
    this.mesas = mesas;
    // console.log(this.mesas[0].numero);
  });
}

tomar(mesa: Mesa){
 mesa.habilitada = false;
 mesa.estado = MesaEstados.ocupada;
 this.actualizarEstadoMesa(mesa);
}

liberar(mesa: Mesa){
  mesa.habilitada = true;
  mesa.estado = MesaEstados.libre;
  this.actualizarEstadoMesa(mesa);
}
actualizarEstadoMesa(mesa: Mesa){
this.mesaServicio.modificar(mesa);
}


}
