import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MesasEstadosComponent } from './mesas-estados.component';

describe('MesasEstadosComponent', () => {
  let component: MesasEstadosComponent;
  let fixture: ComponentFixture<MesasEstadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MesasEstadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MesasEstadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
