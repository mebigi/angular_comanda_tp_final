import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MesasEstadisticaComponent } from './mesas-estadistica.component';

describe('MesasEstadisticaComponent', () => {
  let component: MesasEstadisticaComponent;
  let fixture: ComponentFixture<MesasEstadisticaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MesasEstadisticaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MesasEstadisticaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
