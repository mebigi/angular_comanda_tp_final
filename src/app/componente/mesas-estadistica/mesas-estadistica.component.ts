import { Component, OnInit } from '@angular/core';
import { FotoService } from 'src/app/servicios/foto.service';
import { DatePipe } from '@angular/common';
import { PedidoService } from 'src/app/servicios/pedido.service';
import * as jsonexport from "jsonexport/dist"
import { saveAs } from 'file-saver';
import { RangoFechaPipe } from 'src/app/pipes/rango-fecha.pipe';
import { MesaService } from 'src/app/servicios/mesa.service';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';


@Component({
  selector: 'app-mesas-estadistica',
  templateUrl: './mesas-estadistica.component.html',
  styleUrls: ['./mesas-estadistica.component.css']
})
export class MesasEstadisticaComponent implements OnInit {

  barChartOptions: ChartOptions = {
    responsive: true,
  };
  barChartLabels: Label[] = [];
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartPlugins = [];

  barChartData: ChartDataSets[] = [
    { data: [], label: '' },
    { data: [], label: '' }
  ];

  ventas: any[] = [];
  Fechavi: any[] = [];
  Fechavf: any[] = [];
  ventasMozos: any[] = [];
  ventasBartenders: any[] = [];
  ventasCocineros: any[] = [];
  bartenders: any[] = [];
  cocineros: any[] = [];
  filtro: any;
  arCsv: any[] = [];

  bartenderCantidad: { dia: string, empleado: string, cantidad: number }[] = [];
  cocineroCantidad: { dia: string, empleado: string, cantidad: number }[] = [];
  mozos: any[] = [];
  mozosVentas: any[] = [];
  ventasFulano: any[] = [];
  fulano: string;
  diaf: any;
  dia: any;
  max: any = [];
  min: { numero: any, cantidad: number } = { numero: 0, cantidad: 0 }

  productoCantidad: any[] = [];
  productos: any[] = [];
  ventasFitradas: any[] = [];
  noatiempo: any[] = [];
  cancelados: any[];
  mesas: any[];
  mesaCantidad: any[] = [];
  mesaImporteFecha: any[] = [];
  max_importe: any = [];
  min_importe: any = [];
  mesaImporteTotal: any[] = [];
  max_importe_total: any;
  min_importe_total: any;
  comentarios: any[] = [];
  max_comentario: any = [];
  min_comentario: any = [];
  mesas_puntaje_max_min: any[] = [];

  constructor(private pedidoServicio: PedidoService, private mesaServicio: MesaService) { }

  ngOnInit() {

    //      15
    let pipe2: DatePipe = new DatePipe('en-US');

    this.mesaServicio.traerTodasMesas().subscribe(mesas => {
      this.mesas = mesas;
      console.log("las mesas", this.mesas);
      this.pedidoServicio.traerTodos('pedidos').subscribe(ventas => {

        this.ventas = ventas;

        this.ventas.forEach(venta => { venta['dia'] = pipe2.transform(venta.fecha_listo, 'MM/dd/yyyy'); venta['diaf'] = pipe2.transform(venta.fecha_listo, 'MM/dd/yyyy'); })
        this.Fechavi = this.getUnique(this.ventas, 'dia');
        this.Fechavf = this.getUnique(this.ventas, 'dia');
        this.diaf = this.Fechavi[0].dia;
        this.dia = this.Fechavi[(this.Fechavi.length - 1)].dia;

        // this.ventasFitradas = this.ventas.filter(venta => new Date(venta.fecha_listo).getTime() >= new Date(this.dia).getTime() && new Date(venta.fecha_listo).getTime() <= new Date(this.diaf).getTime())

        this.ventasFitradas = this.ventas;

        if (this.ventasFitradas.length > 0) {


          this.mesas.forEach(mesa => {
            let p = {};
            p['cantidad'] =
              this.cantidadMesa(mesa.numero, this.ventasFitradas);
            p['numero'] =
              mesa.numero;

            this.mesaCantidad.push(p);
          }
          );

          this.max = this.mesaCantidad.reduce((maximo, actual) => {
            if (actual.cantidad > maximo.cantidad) {
              maximo = actual;
            }
            return maximo
          });

          console.log(" la mas usada max?", this.max);
          this.min = this.mesaCantidad.reduce((maximo, actual) => {
            if (actual.cantidad < maximo.cantidad) {
              maximo = actual;
            }
            return maximo
          });

          console.log("la menos usada min?", this.min);



          this.ventasFitradas.forEach(venta => {
            let p = {};
            p['importe'] = venta.importe_total;
            p['numero'] = venta.mesas[0].numero;
            p['dia'] = pipe2.transform(venta.fecha_listo, 'MM/dd/yyyy');
            p['diaf'] = pipe2.transform(venta.fecha_listo, 'MM/dd/yyyy');

            this.mesaImporteFecha.push(p);
          }
          );

          console.log("this.mesaImporteFecha", this.mesaImporteFecha)



          this.max_importe = this.mesaImporteFecha.reduce((maximo, actual) => {
            if (actual.importe > maximo.importe) {
              maximo = actual;
            }
            return maximo
          });

          console.log(" max importe?", this.max_importe);

          this.min_importe = this.mesaImporteFecha.reduce((maximo, actual) => {
            if (actual.importe < maximo.importe) {
              maximo = actual;
            }
            return maximo
          });

          console.log(" min importe?", this.min_importe);

          this.mesas.forEach(mesa => {
            let importeTotal = 0
            this.mesaImporteFecha.forEach(mesaIF => {
              if (mesaIF.numero == mesa.numero) {
                importeTotal = importeTotal + mesaIF.importe;
              }
            });
            let i = {};
            i['numero'] = mesa.numero;
            i['importeTotal'] = importeTotal;
            this.mesaImporteTotal.push(i);
          }

          );

          this.max_importe_total = this.mesaImporteTotal.reduce((maximo, actual) => {
            if (actual.importeTotal > maximo.importeTotal) {
              maximo = actual;
            }
            return maximo
          });

          console.log(" max importe total?", this.max_importe_total);

          this.min_importe_total = this.mesaImporteTotal.reduce((maximo, actual) => {
            if (actual.importeTotal < maximo.importeTotal) {
              maximo = actual;
            }
            return maximo
          });

          console.log(" mim importe total?", this.min_importe_total);


          this.mesas.forEach(mesa => {
            this.mesaServicio.traerComentarios('mesa_' + mesa.numero).subscribe(comentarios => {
              comentarios.forEach(comentario => {
                let c = {};
                c['numero'] = comentario.id_mesa;
                c['puntaje'] = comentario.puntaje;
                this.comentarios.push(c);
                this.max_comentario = this.comentarios.reduce((maximo, actual) => {
                  if (actual.puntaje > maximo.puntaje) {
                    maximo = actual;
                  }
                  return maximo
                });
                this.min_comentario = this.comentarios.reduce((maximo, actual) => {
                  if (actual.puntaje < maximo.puntaje) {
                    maximo = actual;
                  }
                  return maximo
                });

              });
            }
            );
          })
          this.mesaServicio.traerComentarios('mesa_2').subscribe(comentarios => {console.log("comentarios 1", comentarios)
          let d = {};
          d['numero'] = 1;
          d['mejor_puntaje'] = comentarios.reduce((maximo, actual) => {
            if (actual.puntaje >= maximo.puntaje) {
              maximo = actual;
            }
            return maximo.puntaje
          });
          d['peor_puntaje'] = comentarios.reduce((maximo, actual) => {
            if (actual.puntaje <= maximo.puntaje) {
              maximo = actual;
            }
            return maximo.puntaje
          });
        
          console.log("comentarios 2d", d);
        });

        this.mesaServicio.traerComentarios('mesa_1').subscribe(comentarios => {console.log("comentarios 1", comentarios)
        let d = {};
        d['numero'] = 1;
        d['mejor_puntaje'] = comentarios.reduce((maximo, actual) => {
          if (actual.puntaje >= maximo.puntaje) {
            maximo = actual;
          }
          return maximo.puntaje
        });
        d['peor_puntaje'] = comentarios.reduce((maximo, actual) => {
          if (actual.puntaje <= maximo.puntaje) {
            maximo = actual;
          }
          return maximo.puntaje
        });
      
        console.log("comentarios 1d", d);
      });


          this.mesas.forEach(mesa => {
            this.mesaServicio.traerComentarios('mesa_' + mesa.numero).subscribe(comentarios => {
              if(comentarios.length >0){
                let d = {};
                d['numero'] = mesa.numero;
                d['mejor_puntaje'] = comentarios.reduce((maximo, actual) => {
                  if (actual.puntaje >= maximo.puntaje) {
                    maximo = actual;
                  }
                  return maximo.puntaje
                });
                d['peor_puntaje'] = comentarios.reduce((maximo, actual) => {
                  if (actual.puntaje <= maximo.puntaje) {
                    maximo = actual;
                  }
                  return maximo.puntaje
                });
                this.mesas_puntaje_max_min.push(d);
                console.log("this.mesas_puntaje_max_min", this.mesas_puntaje_max_min);
                this.barChartLabels = this.mesas_puntaje_max_min.map(m => 'mesa ' + m.numero);
                let mejoresp = this.mesas_puntaje_max_min.map(m => m.mejor_puntaje);
                console.log("mejores", mejoresp);
                let peores = this.mesas_puntaje_max_min.map(m => m.peor_puntaje);
                this.barChartData = [
                  { data: peores, label: 'Peores puntajes' },
                  { data: mejoresp, label: 'Mejores puntajes' }
                ];
  
  
              }
            
            }
            );
          }



          )


        }



      }
      );
    }
    );



  }


  getUnique(arr, comp) {
    var unique = arr
      .map(function (e) { return e[comp]; })
      // store the keys of the unique objects
      .map(function (e, i, final) { return final.indexOf(e) === i && i; })
      // eliminate the dead keys & store unique objects
      .filter(function (e) { return arr[e]; }).map(function (e) { return arr[e]; });
    return unique;
  }


  exportCSV(myData: any[], dato: any) {
    //let myData= JSON.stringify(Data);   

    let filtado = new RangoFechaPipe();
    // filtado.transform(this.ventas, dato);

    jsonexport(filtado.transform(this.ventas, dato), function (err, csv) {
      if (err) return console.log(err);
      console.log(csv);

      let file = new Blob([csv], { type: 'text/csv;charset=utf-8' });
      saveAs(file, 'operaciones.csv')

    });
  }

  cargarOperacionesEmpleado(email: string) {
    this.fulano = email;
    this.ventasFulano = this.ventas.filter(venta => venta.empleado == email);

  }



  cantidad(producto: any, ventas: any[]): any {
    let cantidad = 0;
    ventas.forEach(item => {
      if (producto == item.codigo_porducto) {
        cantidad = cantidad + item.cantidad;
      }

    }
    );

    console.log(producto, ' ', cantidad);
    return cantidad;
  }


  cantidadMesa(mesa: any, pedidos: any[]): any {
    let cantidad = 0;
    pedidos.forEach(item => {
      if (mesa == item.mesas[0].numero) {
        cantidad = cantidad + 1;
      }

    }
    );
    console.log(mesa, ' ', cantidad);
    return cantidad;
  }



  mapProductoCantidad(venta: any): any {
    let b = {};
    let pipe2 = new DatePipe('en-US')
    b['dia'] = pipe2.transform(venta.fecha_listo, 'MM/dd/yyyy');
    b['diaf'] = pipe2.transform(venta.fecha_listo, 'MM/dd/yyyy');
    b['tomado'] = venta.fecha_tomado;
    b['listo'] = venta.fecha_listo;
    b['estimado'] = venta.tiempo_estimado;
    b['producto'] = venta.codigo_porducto;
    b['nombre'] = venta.nombre;
    b['cantidad'] = venta.cantidad;
    let diff: number = ((new Date(venta.fecha_listo).getTime()) - (new Date(venta.fecha_tomado).getTime()))
    if (diff != 0) {
      diff = diff / 1000;
      diff /= 60;
      console.log(diff);
    }
    if (diff != NaN && diff != undefined && diff <= venta.tiempo_estimado) {
      b['atiempo'] = true;
    }
    else {
      b['atiempo'] = false;
    }

    console.log('b', b);
    return b;

  }


  MaxReducer(reading) {
    return Math.max(reading);
  }

  MinReducer(reading) {
    return Math.min(reading);
  }


}
