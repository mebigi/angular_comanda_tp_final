import { Component, OnInit, Input, EventEmitter } from '@angular/core';
import { Usuario } from 'src/app/clases/usuario';
import { AuthService } from 'src/app/servicios/auth.service';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';
import { FotoService } from 'src/app/servicios/foto.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
 
})
export class MenuComponent implements OnInit {
  @Input() spinner: boolean;
  items: any[] = [1, 2, 3, 4, 5, 5];
  usuario: Usuario;
  perfil: string;
  
  opts: ISlimScrollOptions;
  scrollEvents: EventEmitter<SlimScrollEvent>;
  constructor(public authservicio: AuthService, public usuarioServicio: FotoService, private route: Router) {
    this.usuario = this.authservicio.usuarioLoguiado;
    this.perfil = this.authservicio.usuarioLoguiado.perfil;

  }



 
  ngOnInit() {
    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.opts = {
      position:'right' ,
      barBackground: '#C9C9C9', // 
      barOpacity: '0.8', // 0.8
      barWidth: '10', // 10
      barBorderRadius:'20', // 20
      barMargin:'0', // 0
      gridBackground: '#D9D9D9', // #D9D9D9
      gridOpacity: '1', // 1
      gridWidth: '2', // 2
      gridBorderRadius: '20', // 20
      gridMargin: '0', // 0
      alwaysVisible:true, // true
      visibleTimeout: 1000, // 1000
     
    }
 
    this.play();
  }
 
  play(): void {
    let event = null;
 
    Promise.resolve()
      .then(() => this.timeout(1500))
      .then(() => {
        event = new SlimScrollEvent({
          type: 'scrollToBottom',
          duration: 1500,
          easing: 'inOutQuad'
        });
 
        this.scrollEvents.emit(event);
      })
      .then(() => this.timeout(1500))
      .then(() => {
        event = new SlimScrollEvent({
          type: 'scrollToTop',
          duration: 1500,
          easing: 'outCubic'
        });
 
        this.scrollEvents.emit(event);
      })
      
     
  }

  logout(){
    this.authservicio.SignOut();
    
  }


  miCuenta(){
    this.usuarioServicio.verDetalle(this.usuario, 'profile').subscribe(resp => this.route.navigate(['Usuario-mod']));

  }
 
  timeout(ms: number): Promise<void> {
    return new Promise(resolve => setTimeout(() => resolve(), ms));
  }
}
 

