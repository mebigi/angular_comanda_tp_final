import { Component, OnInit } from '@angular/core';
import { Producto } from 'src/app/clases/producto';
import { AuthService } from 'src/app/servicios/auth.service';
import { ProductoService } from 'src/app/servicios/producto.service';
import { Usuario } from 'src/app/clases/usuario';
import { FotoService } from 'src/app/servicios/foto.service';
import { FormBuilder, Validators } from '@angular/forms';
import swal from 'sweetalert';
import { Router } from '@angular/router';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {
  public nuevoProducto: Producto;
  public archivo: File;
  files: File[] = [];

  usuario: Usuario;
  perfil: string;
  formAlta: any;


 

  constructor(private route: Router, public formBuilder: FormBuilder, private serviciousuarios: FotoService,  public authservice: AuthService, public productoservice: ProductoService) { 
    this.usuario = this.authservice.usuarioLoguiado;
    this.perfil = this.authservice.usuarioLoguiado.perfil;
    console.log('perfil', this.perfil);
    this.nuevoProducto = new Producto();
  }

  ngOnInit() {
    this.formAlta = this.formBuilder.group(
      {
        nombre: ['', Validators.compose([Validators.maxLength(30), Validators.minLength(2), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
        costo: ['', Validators.compose([Validators.required])],
        categoria: ['', Validators.compose([Validators.maxLength(15), Validators.minLength(3), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      descripcion: ['', Validators.compose([Validators.maxLength(66), Validators.minLength(3), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      });
  }

 	onSelect($event) {
		console.log($event);
		this.files.push(...$event.addedFiles);
    this.archivo = this.files[0];
	}

	onRemove($event) {
		console.log($event);
		this.files.splice(this.files.indexOf($event), 1);
  }
  
  registrarProducto(){
    if (this.archivo != undefined) {
    this.nuevoProducto.cantidad=1;
    //lo agrego parar registro de login
    //this.authservice.SignUp(this.nuevoProducto);
    //lo agrego a la base y subo la fotos al storage
    if(this.nuevoProducto.categoria == "Bebida"){
      this.nuevoProducto.tipo_empleado = "Bartender"
    } else {
      this.nuevoProducto.tipo_empleado = "Cocinero"
    }
    this.productoservice.subir(this.archivo.name, this.archivo, this.nuevoProducto).then(resp => this.route.navigate(['Listado-Productos']));
  }else {
    swal("Debe Adjuntar una foto", "Click para continuar", "error");
  }
  }
 


public getError(form: any, controlName: string): string {
  let error: any;
  let mse = "";
  const control = form.get(controlName);
  if (control.touched && control.errors != null) {
    console.info(JSON.stringify(control.errors));
    error = JSON.parse(JSON.stringify(control.errors));
    if (error.required) {
      mse = "Campo requerido.";
    }
    if (error.minlength != undefined) {
      mse = "Error en logintud mínima requerida.";
    }
    if (error.maxlength != undefined) {
      mse = "Error en la longitud máxima.";
    }

    if (error.pattern != undefined) {
      mse = "Error en el tipo de dato.";
    }
  }
  return mse;
}

}


