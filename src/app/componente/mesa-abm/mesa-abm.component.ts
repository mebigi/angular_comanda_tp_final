import { Component, OnInit } from '@angular/core';
import { Mesa } from 'src/app/clases/mesa';
import { MesaService } from 'src/app/servicios/mesa.service';
import { Usuario } from 'src/app/clases/usuario';
import swal2 from 'sweetalert2';
import { MesaEstados } from 'src/app/enumerados/mesa-estados.enum';

@Component({
  selector: 'app-mesa-abm',
  templateUrl: './mesa-abm.component.html',
  styleUrls: ['./mesa-abm.component.css']
})

export class MesaAbmComponent implements OnInit {
  modificacion: boolean;
  mesa: Mesa = new Mesa();
  public Archivofoto:File;
  ok: boolean = true;
  parse: any;
  mesas: Mesa[];
  mensaje: string = " ";
  image: any;
  mifoto: any;
  boton: HTMLElement;
  menu_b: boolean = true;
  alta: boolean = true;
  baja: boolean;
  prueba:boolean;
  public archivo: File;
  files: File[] = [];
  usuario: Usuario;
  perfil: string;

 


  constructor(public mesaServicio: MesaService) { }

  ngOnInit() {
    
    this.traerTodasMesas();
    /*  this.mesaServicio.traerUnaMesa('mesa_3').subscribe(mesa => {
        console.log(mesa.numero);
     });
     */
   // this.traerMesa(3);
  }

  onSelect($event) {
		console.log($event);
		this.files.push(...$event.addedFiles);
    this.Archivofoto = this.files[0];
	}

	onRemove($event) {
		console.log($event);
		this.files.splice(this.files.indexOf($event), 1);
  }
  


  traerTodasMesas(){
  this.mesaServicio.traerTodasMesas()
  .subscribe(mesas => {
    this.mesas = mesas;
    // console.log(this.mesas[0].numero);
  });
}




  altaMesa() {
    this.mensaje = " ";
     this.estAnim('alta', 'animation-target'); 
    if (this.mesas.some(mesa => mesa.numero == this.mesa.numero)) {
      this.mensaje = "la mesa ya existe";
     
    swal2.fire({
      icon: 'error',
      title: 'la mesa ya existe!',
      text: 'Ingrese otra mesa',
    });
 
      console.log('la mesa ya existe');
    } else if (this.mesa.numero == undefined) {
      this.mensaje = "Ingrese una la mesa válida";
      swal2.fire({
        icon: 'error',
        title: 'la mesa inválida',
        text: 'Ingrese una la mesa válida',
      });
   
    }else {
      //if si no esta primero traertodos 
      if (this.Archivofoto != undefined) {
        console.info(this.Archivofoto);
        this.mesa.estado = MesaEstados.libre;
        this.mesaServicio.altaMesa(this.Archivofoto.name, this.Archivofoto, this.mesa);
        this.mensaje = ("mesa cargada");
        swal2.fire({
          title: 'Registro Exitoso!',
          text: 'Se guardo la Mesa',
          icon: 'success',
          confirmButtonText: 'ok'
        })
        this.traerTodasMesas();
        this.mesa = new Mesa();
      }
      else {
        this.mensaje = ("imagen no cargada");
        swal2.fire({
          title: 'Error!',
          text: 'imagen no cargada',
          icon: 'error',
          confirmButtonText: 'ok'
        })
       
      }
    }
  }

vibrar(){
  
}

 


  bajaMesa() {
    this.mensaje = " ";
    this.estAnim('baja', 'animation-target');
    if (this.mesas.some(mesa => mesa.numero == this.mesa.numero)) {
      this.mesa.baja = true;
      this.mesaServicio.bajaMesa(this.mesa); 
      this.mensaje = ("Se dió de baja la mesa nro " + this.mesa.numero.toString());
      this.traerTodasMesas();
    } else {
      this.mensaje = ("No se encontró la mesa indicada");
      
    }
    this.mesa = new Mesa();
    }
  
    

  estAnim(elementId, animClasses) {
    document.getElementById(elementId).classList.add(animClasses);
    var wait = window.setTimeout(function () {
      document.getElementById(elementId).classList.remove(animClasses)
    },
      1300
    );
  }

  modificarMesa() {
    this.mensaje = " ";
    this.estAnim('modificar', 'animation-target');
    if (this.mesas.some(mesa => mesa.numero == this.mesa.numero)) {
      this.mesaServicio.modificarMesa(this.mesa);
      this.mensaje = ("Se modificó la mesa nro " + this.mesa.numero.toString());
      this.traerTodasMesas();
    } else {
      this.mensaje = ("No se encontró la mesa indicada");
    
    
    }
    this.mesa = new Mesa();
   
  }


  traerMesa(numero: number) {
    let uid: string = 'mesa_' + numero.toString();
    this.mesaServicio.traerUnaMesa(uid).subscribe(mesa => {
     this.mesa = mesa;

  
    });
   
  }

  menu(accion: string) {
    switch (accion) {
      case 'alta':
        this.alta = true;
        this.baja = false;
        this.modificacion = false;
        this.mensaje=" ";
        break;
      case 'baja':
        this.baja = true;
        this.alta = false;
        this.modificacion = false;
        this.mensaje=" ";
        break;
      case 'modificacion':
        this.modificacion = true;
        this.baja = false;
        this.alta = false;
        this.mensaje=" ";
        break;
    }
  }


}

