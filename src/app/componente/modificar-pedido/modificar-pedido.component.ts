import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Pedido } from 'src/app/clases/pedido';
import { EstadoPedido } from 'src/app/enumerados/estado-pedido.enum';

import swal2 from 'sweetalert2';

import { Routes, ActivatedRoute } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ProductoPedido } from 'src/app/clases/producto-pedido';
import { MesaEstados } from 'src/app/enumerados/mesa-estados.enum';
import { MesaService } from 'src/app/servicios/mesa.service';
import { Mesa } from 'src/app/clases/mesa';
import { PedidoService } from 'src/app/servicios/pedido.service';
import { ProductoService } from 'src/app/servicios/producto.service';
import { AuthService } from 'src/app/servicios/auth.service';


@Component({
  selector: 'app-modificar-pedido',
  templateUrl: './modificar-pedido.component.html',
  styleUrls: ['./modificar-pedido.component.css']
})
export class ModificarPedidoComponent implements OnInit {
  pedido_iniciado: boolean;
  pedido_completado: boolean;

  @Input() pedido: Pedido;
  @Output() seModifico: EventEmitter<any> = new EventEmitter()
  @Input() modificar: any;
  spinner: boolean;
  productosPedidos: any[];
  costoTotal: number;
  ahora: Date;
  productos: any[];
  categorias: any[] = [];
  constructor(public authservicio: AuthService, private servicioproductos: ProductoService, public pedidoServicio: PedidoService, private mesasServicio: MesaService) { }

  ngOnInit() {
    this.traerTodosProductos();
    this.pedidoServicio.updateTime().then(res => this.pedidoServicio.traerFecha().subscribe((doc) => {
      this.ahora = new Date(doc.data().hoy.toDate());
      console.log(this.ahora);

    }));
  }


  traerTodosProductos() {
    this.servicioproductos.traerTodos('productos')
      .subscribe(productos => {
        this.productos = productos.filter(p=> p.baja != true);
      });
  }


  quitarProductoPedido(pedido: Pedido, producto: any) {
    const index = pedido.productos.indexOf(producto);
    pedido.productos.splice(index, 1);
    pedido.importe_total = pedido.importe_total - (producto.costo * producto.cantidad);

  }


  agregarProductoPedido(pedido: Pedido, producto: any) {
    //this.paisesService.verDetalle(pais).subscribe(resp => resp);
    if (producto.cantidad > 0) {
      producto.monto = producto.costo * producto.cantidad;
      pedido.productos.push(producto);
      pedido.importe_total = pedido.importe_total + (producto.costo * producto.cantidad);

    }
    else {
      //ingrese cantidad

    }
  }


  ModificarPedido(pedido: Pedido) {
    this.seModifico.emit(true);console.log("estoy emitiendo");
    this.authservicio.spinner= true;

    if (pedido.estado == 'iniciado') {
      pedido.estado = EstadoPedido.pendiente;
      pedido.productos.forEach(item => item.estado = EstadoPedido.pendiente);
      pedido.mesas.forEach(item => item.estado = MesaEstados.ocupada);
      //subir cada mesa
    }
    else {
      if (pedido.estado != 'finalizado') {
        if (this.pedidoEstaListo(pedido)) {
          pedido.estado = EstadoPedido.listoaraservir;
          pedido.fechaListo = this.ahora;

        }
        else if (this.pedidoServido(pedido)) {
          pedido.estado = EstadoPedido.servido;
          pedido.mesas.forEach(item => item.estado = MesaEstados.comiendo);

        }
      }
      else {
        //finalizado -> pagando
        pedido.mesas.forEach(item => item.estado = MesaEstados.pagando);

      }
    }

   
    let respuesta = this.pedidoServicio.modificar(pedido).then((res) => {
      this.actualizarEstadoMesas(pedido.mesas)
     
      console.log("modificque!");
    
    }).finally(() =>  { this.authservicio.spinner= false; });


  }

  actualizarEstadoMesas(mesas: Mesa[]) {
    mesas.forEach(item => this.mesasServicio.modificar(item));

  }

  CancelarModificarPedido() {
    this.seModifico.emit(true);
  }




  hayProductosPendientes(pedido: Pedido): boolean {
    let retorno: boolean = false;
    pedido.productos.forEach(producto => {
      if (producto.estado == EstadoPedido.pendiente) {
        retorno = true;
      }
    });

    return retorno;
  }

  //chequea si algun producto esta distinto a listo
  pedidoEstaListo(pedido: Pedido): boolean {
    let mapeados = pedido.productos
      .filter(function (e) { return e['estado'] == EstadoPedido.listoaraservir; });

    if (pedido.productos.length == mapeados.length) {
      swal2.fire(
        'listoo!',
        'prductos:' + pedido.productos.length + ' mapeados:' + mapeados.length,
        'success'
      )
      return true;
    }
    else {
      return false;
    }

  }

  //chequea si algun producto esta distinto a servido
  pedidoServido(pedido: Pedido): boolean {
    let mapeados = pedido.productos
      .filter(function (e) { return e['estado'] == EstadoPedido.servido; });
    if (pedido.productos.length == mapeados.length) {
      return true;
    }
    else {
      return false;
    }
  }

}
