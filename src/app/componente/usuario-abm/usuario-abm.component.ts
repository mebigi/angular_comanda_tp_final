import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/servicios/auth.service';
import { Usuario } from 'src/app/clases/usuario';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { FotoService } from 'src/app/servicios/foto.service';
import { ProductoService } from 'src/app/servicios/producto.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import swal from 'sweetalert';

@Component({
  selector: 'app-usuario-abm',
  templateUrl: './usuario-abm.component.html',
  styleUrls: ['./usuario-abm.component.css']
})
export class UsuarioAbmComponent implements OnInit {
  public nuevoUsuario: Usuario = new Usuario();
  public archivo: File;
  files: File[] = [];

  perfil: string;
  locales: any[];
  modificacion: boolean;

  usuario: Usuario = new Usuario();
  public Archivofoto: any;
  ok: boolean = true;
  parse: any;
  usuarios: Usuario[];
  mensaje: string = " ";
  image: any;
  mifoto: any;
  boton: HTMLElement;
  menu_b: boolean = true;
  alta: boolean = true;
  baja: boolean;
  prueba: boolean;
  public formAlta: FormGroup;
  formBaja: FormGroup;
  formMod: FormGroup;
  amodificar:any


  constructor(public formBuilder: FormBuilder, public formBuilder2: FormBuilder, public formBuilder3: FormBuilder, public usuarioServicio: FotoService, private servicioproductos: ProductoService, private authservice: AuthService, public fotoservice: FotoService) {
    this.usuario = this.authservice.usuarioLoguiado;
    this.perfil = this.authservice.usuarioLoguiado.perfil;
    this.nuevoUsuario = new Usuario();

  }

  ngOnInit() {
    this.cargarform();
    this.traerTodasUsuarios();
   
 this.usuarioServicio.traerUsuariouid('usuario_13969584').subscribe((res)=> {this.amodificar = res;  
   console.log(this.amodificar);});
   
  }


  async  createFile(url:string){
    let response = await fetch(url);
    let data = await response.blob();
    let metadata = {
      type: 'image/jpeg'
    };
    return new File([data], "foto.jpg", metadata);
    // ... do something with the file or return it
  }


  onSelect($event) {
    console.log($event);
    this.files.push(...$event.addedFiles);
    this.archivo = this.files[0];
  }

  onRemove($event) {
    console.log($event);
    this.files.splice(this.files.indexOf($event), 1);
  }

  registrarUsuario() {
    console.log("sucursal:", this.nuevoUsuario.sucursal);
    //lo agrego parar registro de login
    this.authservice.SignUp(this.nuevoUsuario);
    //lo agrego a la base y subo la fotos al storage
    this.fotoservice.subir(this.archivo.name, this.archivo, this.nuevoUsuario);
  }

  //var file = campoArchivo.get(0).files[0];
  //subir(filename: string, file: any, perfil: string, usuario: Usuario) {}


  cargarform() {
    this.formAlta = this.formBuilder.group(
      {
        nombre: ['', Validators.compose([Validators.maxLength(30), Validators.minLength(2), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
        apellido: ['', Validators.compose([Validators.maxLength(30), Validators.minLength(2), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
        dni: ['', Validators.compose([Validators.maxLength(10), Validators.minLength(7), Validators.pattern('[0-9]*'), Validators.required])],
        perfil: ['', Validators.compose([Validators.required])],
        email: ['', Validators.compose([Validators.email, Validators.required])],
        clave: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
      });
    this.formBaja = this.formBuilder2.group(
      {
        dniBaja: ['', Validators.compose([Validators.maxLength(10), Validators.minLength(7), Validators.pattern('[0-9]*'), Validators.required])],
        motivoBaja: ['', Validators.compose([Validators.minLength(6), Validators.required])]
      });
    this.formMod = this.formBuilder3.group(
      {
        nombreMod: ['', Validators.compose([Validators.maxLength(30), Validators.minLength(2), Validators.pattern('[a-zA-Z ]*')])],
        apellidoMod: ['', Validators.compose([Validators.maxLength(30), Validators.minLength(2), Validators.pattern('[a-zA-Z ]*')])],
        dniMod: ['', Validators.compose([Validators.maxLength(10), Validators.minLength(7), Validators.pattern('[0-9]*'), Validators.required])],
        emailMod: ['', Validators.compose([Validators.email])],
      });
  }

  public getError(form: any, controlName: string): string {
    let error: any;
    let mse = "";
    const control = form.get(controlName);
    if (control.touched && control.errors != null) {
      console.info(JSON.stringify(control.errors));
      error = JSON.parse(JSON.stringify(control.errors));
      if (error.required) {
        mse = "Campo requerido.";
      }
      if (error.minlength != undefined) {
        mse = "Error en logintud mínima requerida.";
      }
      if (error.maxlength != undefined) {
        mse = "Error en la longitud máxima.";
      }

      if (error.pattern != undefined) {
        mse = "Error en el tipo de dato.";
      }
    }
    return mse;
  }


  traerTodasUsuarios() {
    this.usuarioServicio.traerTodos()
      .subscribe(usuarios => {
        this.usuarios = usuarios;
        // console.log(this.usuarios[0].numero);
      });
  }


  altaUsuario() {
    this.mensaje = " ";
    this.estAnim('alta', 'animation-target');
    if (this.archivo != undefined) {

      if (this.usuarios.some(usuario => usuario.dni == this.nuevoUsuario.dni) && this.nuevoUsuario.dni != undefined) {
        this.mensaje = "la usuario ya existe";
        swal(this.mensaje);
        console.log('la usuario ya existe');
      } else {
        this.authservice.SignUp(this.nuevoUsuario).then(() => this.fotoservice.subir(this.archivo.name, this.archivo, this.nuevoUsuario).then(() => {
          swal("Registro Existoso!", "Click para continuar", "success");
          this.mensaje = ("usuario cargada");
        }
        ))

      }
    } else {
      swal("Debe Adjuntar una foto", "Click para continuar", "error");
    }
  }





  bajaUsuario() {
    this.mensaje = " ";
    this.estAnim('baja', 'animation-target');
    if (this.usuarios.some(usuario => usuario.dni == this.nuevoUsuario.dni) && this.nuevoUsuario.baja == false) {
      this.nuevoUsuario.baja = true;
      this.usuarioServicio.bajaUsuario(this.nuevoUsuario);

      this.mensaje = ("Se dió de baja la usuario nro " + this.nuevoUsuario.dni.toString());
      swal("Baja exitosa!", this.mensaje, "success");
      this.traerTodasUsuarios();
      this.cargarform();
    } else {
      swal("Error", "Usuario no encontrado", "error");

    }
    this.nuevoUsuario = new Usuario();
  }







  estAnim(elementId, animClasses) {
    document.getElementById(elementId).classList.add(animClasses);
    var wait = window.setTimeout(function () {
      document.getElementById(elementId).classList.remove(animClasses)
    },
      1300
    );
  }

  modificarUsuario() {
    this.mensaje = " ";
    this.estAnim('modificar', 'animation-target');

    
    if (this.usuarios.some(usuario => usuario.dni == this.amodificar.dni)) {
         
      
      this.usuarioServicio.modificarUsuario(this.amodificar);
      this.mensaje = ("Se modificó la usuario nro " + this.amodificar.dni.toString());
      if (this.archivo != undefined) {
        this.usuarioServicio.subirFoto(this.archivo.name, this.archivo, 'usuario_'+this.amodificar.dni)
      }
      swal("Modificación exitosa!", this.mensaje, "success");
      this.traerTodasUsuarios();
           } else {
      this.mensaje = ("No se encontró le usuarie indicade");
      swal("Error", this.mensaje, "error");
    }
   

  }


  traerUsuario(numero: number) {
    let uid: string = 'usuario_' + numero.toString();
    this.usuarioServicio.traerUsuariouid(uid).subscribe(usuario => {
      this.nuevoUsuario = usuario;
    });

  }

  menu(accion: string) {
    switch (accion) {
      case 'alta':
        this.alta = true;
        this.baja = false;
        this.modificacion = false;
        this.mensaje = " ";
        break;
      case 'baja':
        this.baja = true;
        this.alta = false;
        this.modificacion = false;
        this.mensaje = " ";
        break;
      case 'modificacion':
        this.modificacion = true;
        this.baja = false;
        this.alta = false;
        this.mensaje = " ";
        break;
    }
  }



}


