import { Component, OnInit } from '@angular/core';
import { FotoService } from 'src/app/servicios/foto.service';
import { DatePipe } from '@angular/common';
import * as jsonexport from "jsonexport/dist"
import { saveAs } from 'file-saver';
import { RangoFechaPipe } from 'src/app/pipes/rango-fecha.pipe';

@Component({
  selector: 'app-ingresos',
  templateUrl: './ingresos.component.html',
  styleUrls: ['./ingresos.component.css']
})
export class IngresosComponent implements OnInit {
  ingresos: any[]=[];
  Fechai: any[]=[];
  Fechaf: any[]=[];

  constructor(private servicioUsuario: FotoService) { }

  ngOnInit() {
    let pipe: DatePipe = new DatePipe('en-US');
    this.servicioUsuario.traerTodosIngresos().subscribe(ingresos => 
    {this.ingresos = ingresos; console.log(this.ingresos);
      this.ingresos.forEach(ingreso => {ingreso['dia'] =  pipe.transform(ingreso.fecha, 'MM/dd/yyyy');ingreso['diaf'] =  pipe.transform(ingreso.fecha, 'MM/dd/yyyy');})
      this.Fechai = this.getUnique(this.ingresos, 'dia');
      this.Fechaf = this.getUnique(this.ingresos, 'dia');
      console.log(this.Fechai);
    });
   
  }

  
  getUnique(arr, comp) {    
    var unique = arr
        .map(function (e) { return e[comp]; })
        // store the keys of the unique objects
        .map(function (e, i, final) { return final.indexOf(e) === i && i; })
        // eliminate the dead keys & store unique objects
        .filter(function (e) { return arr[e]; }).map(function (e) { return arr[e]; });
    return unique;
}


exportCSV(myData: any[], dato:any) {
  //let myData= JSON.stringify(Data);   

  let filtado= new RangoFechaPipe();
 // filtado.transform(this.ventas, dato);

  jsonexport(filtado.transform(this.ingresos, dato), function (err, csv) {
    if (err) return console.log(err);
    console.log(csv);

    let file = new Blob([csv], { type: 'text/csv;charset=utf-8' });
    saveAs(file, 'ingresos.csv')

  });
}

}
