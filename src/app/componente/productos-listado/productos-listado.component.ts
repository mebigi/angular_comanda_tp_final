import { Component, OnInit } from '@angular/core';
import { FotoService } from 'src/app/servicios/foto.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/servicios/auth.service';
import { Usuario } from 'src/app/clases/usuario';
import { Producto } from 'src/app/clases/producto';
import { ProductoService } from 'src/app/servicios/producto.service';

@Component({
  selector: 'app-productos-listado',
  templateUrl: './productos-listado.component.html',
  styleUrls: ['./productos-listado.component.css']
})
export class ProductosListadoComponent implements OnInit {
  productos: Producto[] = [];
  filtro: any[] = [];
  usuarios: Usuario[] = [];
  usuario: Usuario;
  perfil: string;
  categoria:string = "todas";


  constructor(private servicioproductos: ProductoService, private authservice: AuthService, private serviciousuarios: FotoService, private route: Router) {
    this.usuario = new Usuario;
    this.usuario = this.authservice.usuarioLoguiado;
    this.perfil = this.authservice.usuarioLoguiado.perfil;

  }

  ngOnInit() {
    this.traerTodasUsuarios();
  }

  traerTodasUsuarios() {
    this.servicioproductos.traerTodos('productos')
      .subscribe(productos => {
        this.productos = productos;
      });
  }


  mostrar(producto: Producto) {

    this.servicioproductos.verDetalle(producto).subscribe(resp => this.route.navigate(['Producto-ABM']));
  }


  modificar(producto: Producto) {
     this.servicioproductos.verDetalle(producto, 'modificar').subscribe(resp => this.route.navigate(['Producto-mod']));
  }


  baja(producto: Producto) {
  
    this.servicioproductos.verDetalle(producto, 'baja').subscribe(resp => this.route.navigate(['Producto-mod']));
  }


}
