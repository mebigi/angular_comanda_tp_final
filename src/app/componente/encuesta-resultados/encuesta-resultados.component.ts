import { Component, OnInit, Input } from '@angular/core';
import { StarRatingComponent } from 'ng-starrating';
import { Encuesta } from 'src/app/clases/encuesta';
import { EncuestaService } from 'src/app/servicios/encuesta.service';
import swal2 from 'sweetalert2';
import { PedidoService } from 'src/app/servicios/pedido.service';
import { Pedido } from 'src/app/clases/pedido';
import { AuthService } from 'src/app/servicios/auth.service';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-encuesta-resultados',
  templateUrl: './encuesta-resultados.component.html',
  styleUrls: ['./encuesta-resultados.component.css']
})
export class EncuestaResultadosComponent implements OnInit {

  
  encuestas: Encuesta[] =[];
  resultado: Encuesta;
  texto: string[]=[];
  pipe = new DatePipe('en-US');
 

  constructor(authservivio: AuthService, private EncuestaServicio: EncuestaService,public pedidoServicio: PedidoService) {
    //this.authservivio.usuarioLoguiado.perfil

    //this.resultado = new Encuesta();

    //console.log(this.myFormattedDate);
   }

  ngOnInit() {
    
    
    this.EncuestaServicio.traerTodos('encuestas').subscribe((res)=> {
      this.encuestas = res;
      this.resultado = new Encuesta();
      if(this.encuestas.length > 0){
        this.resultado.texto = "";
      this.encuestas.forEach(encuesta => {       
        this.resultado.PrecioCalidad += (encuesta.PrecioCalidad/this.encuestas.length);
        this.resultado.bartender += (encuesta.bartender/this.encuestas.length);
        this.resultado.mozo += (encuesta.mozo/this.encuestas.length);
        this.resultado.mesa += (encuesta.mesa/this.encuestas.length);
        this.resultado.cocinero += (encuesta.cocinero/this.encuestas.length);
        this.resultado.restaurante += (encuesta.restaurante/this.encuestas.length);
        
        if(encuesta.fecha != undefined && encuesta.texto != undefined && encuesta.texto != "") { 
         let  myFormattedDate = this.pipe.transform(encuesta.fecha, 'dd/MM/yyyy');
         let txt = encuesta.texto+' -'+encuesta.codigo_pedido+' -'+myFormattedDate;
        this.texto.push(txt);
      }

        this.resultado.PrecioCalidad = Math.round(this.resultado.PrecioCalidad * 100) / 100;
       
        this.resultado.bartender = Math.round(this.resultado.bartender * 100) / 100;
        this.resultado.mozo= Math.round(this.resultado.mozo * 100) / 100;
        this.resultado.mesa= Math.round(this.resultado.mesa * 100) / 100;
        this.resultado.cocinero= Math.round(this.resultado.cocinero * 100) / 100;
        this.resultado.restaurante= Math.round(this.resultado.restaurante * 100) / 100;
       
        //this.texto = this.texto.filter(t=> t != '');
       
      
      }
        
        
        
        )
    }})


  }



}


