
import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { ProductoService } from 'src/app/servicios/producto.service';
import { Producto } from 'src/app/clases/producto';
import { Router } from '@angular/router';
import { FotoService } from 'src/app/servicios/foto.service';
import { AuthService } from 'src/app/servicios/auth.service';
import { Usuario } from 'src/app/clases/usuario';
import { Pedido } from 'src/app/clases/pedido';
import { PedidoService } from 'src/app/servicios/pedido.service';
import { EstadoPedido } from 'src/app/enumerados/estado-pedido.enum';
import { NgxPrintModule } from 'ngx-print';



import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import swal2 from 'sweetalert2';

import { Routes, ActivatedRoute } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ProductoPedido } from 'src/app/clases/producto-pedido';
import { MesaEstados } from 'src/app/enumerados/mesa-estados.enum';
import { MesaService } from 'src/app/servicios/mesa.service';
import { Mesa } from 'src/app/clases/mesa';

import jsPDF from 'jspdf';


@Component({
  selector: 'app-pedidos-estados',
  templateUrl: './pedidos-estados.component.html',
  styleUrls: ['./pedidos-estados.component.css']
})
export class PedidosEstadosComponent implements OnInit {
  productos: Producto[] = [];
  filtro: Producto[] = [];
  usuario: Usuario;
  perfil: string;
  pedido: Pedido = new Pedido();
  pedidos: Pedido[];
  productosPedidos: ProductoPedido[] = [];
  mesa: string = "";
  costoTotal: number = 0;
  pedido_iniciado: boolean;
  cliente: string = "";
  codigo: string = " ";
  public spinner: boolean = false;
  modificar: number = -1;
  tiempo_estimado: number;

  private ok: boolean; //Login OK
  private error: boolean; //Login fallido
  public formularioIniciar: FormGroup;
  private errorDatos: boolean; //Error en el formato de datos de correo o clave
  private enEspera: boolean; //Muestra u oculta el spinner
  pedido_completado: boolean;
  //ahora = new Date().toLocaleString();
  ahora: Date;

  public archivo: File;
  files: File[] = [];
  pendientes: boolean;
  enpreparacion: boolean;
  listoparaservir: boolean;
  servido: boolean;
  finalizado: boolean;
  iniciado: boolean;

  @ViewChild('pdfTable', { static: false }) pdfTable: ElementRef;
  name = 'Angular Html To Pdf ';




  constructor(private mesasServicio: MesaService, private router: Router, public formBuilder: FormBuilder, public authservicio: AuthService, private usuarioServicio: FotoService, private authservice: AuthService, private servicioproductos: ProductoService, public pedidoServicio: PedidoService, private route: Router) {
    this.formularioIniciar = this.formBuilder.group(
      {
        mesa: ['', Validators.compose([Validators.required])],
        cliente: ['', Validators.compose([Validators.minLength(2), Validators.required])]
      });
    this.usuario = this.authservice.usuarioLoguiado;
    this.perfil = this.authservice.usuarioLoguiado.perfil;
    console.log('perfil', this.perfil);
    console.log('archivo', this.archivo);
  }



  ngOnInit() {
    this.authservice.spinner = true;
    this.traerTodosProductos();
    this.pedidoServicio.updateTime().then(res => this.pedidoServicio.traerFecha().subscribe((doc) => {
      this.ahora = new Date(doc.data().hoy.toDate());
      console.log(this.ahora);
      this.traerTodosPedidos();
    })).finally(() => this.authservice.spinner = false);

  }


  downloadAsPDF() {
    const doc = new jsPDF();
    var img = new Image()
    img.src = 'assets/logo_tipo.png';
    doc.addImage(img, 'png', 15, 15, 80, 20)

    const specialElementHandlers = {
      '#editor': function (element, renderer) {
        return true;
      }
    };
    const pdfTable = this.pdfTable.nativeElement;
    doc.fromHTML(pdfTable.innerHTML, 15, 35, {
      width: 190,
      'elementHandlers': specialElementHandlers
    });
    doc.save('tableToPdf.pdf');
  }

  modificado($event) {
    if ($event == true) {
      this.modificar = -1;
      console.log("this.modificar", this.modificar)
    }
  }


  onSelect($event) {
    if (this.archivo == undefined) {
      console.log($event);
      this.files.push(...$event.addedFiles);
      this.archivo = this.files[0];
    }

  }

  onRemove($event) {
    console.log($event);
    this.files.splice(this.files.indexOf($event), 1);
    this.archivo = undefined;
  }


  subirFotoPedido() {
    if (this.archivo != undefined) {

      this.pedidoServicio.subirFoto(this.archivo.name, this.archivo, this.pedidoServicio.codigo).then(() => {
        swal2.fire({
          title: 'Registro Exitoso!',
          text: 'Se guardo la Foto',
          icon: 'success',
          confirmButtonText: 'ok'
        })
      })
    } else {
      swal2.fire({
        icon: 'error',
        title: 'Imagen no selecionada...',
        text: 'Olvido adjuntar la foto!',

      })
    }

  }

  public getError(form: any, controlName: string): string {
    let error: any;
    let mse = "";
    const control = form.get(controlName);
    if (control.touched && control.errors != null) {
      console.info(JSON.stringify(control.errors));
      error = JSON.parse(JSON.stringify(control.errors));
      if (error.required) {
        mse = "Campo requerido.";
      }
      if (error.minlength != undefined) {
        mse = "Error en logintud mínima requerida.";
      }
      if (error.maxlength != undefined) {
        mse = "Error en la longitud máxima.";
      }

      if (error.pattern != undefined) {
        mse = "Error en el tipo de dato.";
      }
    }
    return mse;
  }

  public addMinutes(date, minutes) {
    return new Date(new Date(date).getTime() + minutes * 60000);
  }


  public getOk(): boolean {
    return this.ok;
  }

  public getErrorDatos(): boolean {
    return this.errorDatos;
  }

  public getEnEspera(): boolean {
    return this.enEspera;
  }

  getUnique(arr, comp) {
    var unique = arr
      .map(function (e) { return e[comp]; })
      // store the keys of the unique objects
      .map(function (e, i, final) { return final.indexOf(e) === i && i; })
      // eliminate the dead keys & store unique objects
      .filter(function (e) { return arr[e]; }).map(function (e) { return arr[e]; });
    return unique;
  }



  traerTodosPedidos() {
    this.servicioproductos.traerTodos('pedidos')
      .subscribe(pedidos => {
        this.authservice.spinner = false;
        this.pedidos = this.sortByfecha(pedidos);
        console.log("todo pedidos", this.pedidos);
        this.pendientes = this.filtroPedidos(this.pedidos, 'pendiente');
        this.enpreparacion = this.filtroPedidos(this.pedidos, EstadoPedido.enpreparacion);
        this.listoparaservir = this.filtroPedidos(this.pedidos, EstadoPedido.listoaraservir);
        this.servido = this.filtroPedidos(this.pedidos, EstadoPedido.servido);
        this.finalizado = this.filtroPedidos(this.pedidos, EstadoPedido.finalizado);
        this.iniciado = this.filtroPedidos(this.pedidos, EstadoPedido.iniciado);

      });
  }

  private getTime(date?: Date) {
    return date != null ? date.getTime() : 0;
  }


  public sortByfecha(myArray: any[]): any[] {
    return myArray.filter(item => this.getTime(new Date(item.fecha)) - ((this.getTime(this.ahora) - (720 * 60000))) >= 0)
      .sort((a: any, b: any) => {
        return this.getTime(new Date(a.fecha)) - this.getTime(new Date(b.fecha));
      });
  }

  filtroPedidosMozo(pedidos: any[], estado: string): boolean {

    if (pedidos.filter(item => ((item.mozo.perfil == 'Cliente' && estado == EstadoPedido.iniciado && this.authservice.usuarioLoguiado.perfil == 'Mozo') || item.mozo.email == this.authservice.usuarioLoguiado.email || this.authservice.usuarioLoguiado.perfil == 'Admin') && item.estado == estado).length > 0) {
      return true;
    }
    return false;
  }


  filtroPedidos(pedidos: any[], estado: string): boolean {
    //si hay algun producto para el estado solicitado
    let resultado = pedidos.some(pedido => {
      if (pedido.productos.some(producto => producto.estado == estado)) {
        // con esto me aseguro de mostrar todo en admn siempre que coincida el estado de algun prodctu del pedido
        if (this.authservicio.usuarioLoguiado.perfil != 'Admin') {
          console.log("this.segunEmpleado(estado, pedido):", this.segunEmpleado(estado, pedido), 'estado:', estado)
          return this.segunEmpleado(estado, pedido);
        }
        else {
          return true;
        }
      }
      else {
        //si NO hay algun producto, oculto todo
        return false;
      }
    });


    return resultado;
  }





  traerTodosProductos() {
    this.servicioproductos.traerTodos('productos')
      .subscribe(productos => {
        this.productos = productos;


      });
  }




  mostrar(producto: any) {
    //this.paisesService.verDetalle(pais).subscribe(resp => resp);
    this.servicioproductos.verDetalle(producto).subscribe(resp => resp);
  }

  agregarProductoPedido(pedido: Pedido, producto: any) {
    //this.paisesService.verDetalle(pais).subscribe(resp => resp);
    if (producto.cantidad > 0) {
      producto.monto = producto.costo * producto.cantidad;
      pedido.productos.push(producto);
      pedido.importe_total = pedido.importe_total + (producto.costo * producto.cantidad);
    }
    else {
      //ingrese cantidad

    }
  }


  quitarProductoPedido(pedido: Pedido, producto: any) {
    const index = pedido.productos.indexOf(producto);
    pedido.productos.splice(index, 1);
    pedido.importe_total = pedido.importe_total - (producto.costo * producto.cantidad);

  }


  TomarPedidoIniciado(pedido: any) {
    //pase de iniciado a pendiente
    this.pedido.estado = EstadoPedido.pendiente;
    pedido.productos.forEach(producto => {
      producto.mozo = this.authservicio.usuarioLoguiado;
      producto.estado = EstadoPedido.pendiente;
    });

    const data = {
      uid: this.authservicio.usuarioLoguiado.id,
      usuario: this.authservicio.usuarioLoguiado.email,
      nombre: this.authservicio.usuarioLoguiado.nombre,
      perfil: this.authservicio.usuarioLoguiado.perfil,
      codigo: pedido.codigo,
      fecha: this.ahora,
    };

    this.usuarioServicio.addOperaciones('usuarios', this.authservicio.usuarioLoguiado.id, data).then(() => this.ModificarPedido(pedido))

  }


  ModificarPedido(pedido: Pedido) {
    let unico: Producto[] = this.getUnique(pedido.productos, 'estado');
    if (unico.length == 1) {
      pedido.estado = unico[0].estado;
    }
    if (pedido.estado == 'pendiente') {
      pedido.mesas.forEach(item => item.estado = MesaEstados.ocupada);

      pedido.mesas.forEach(mesa => {
        const data = {
          numero: mesa.numero,
          id_mesa: mesa.id,
          fecha: pedido.fecha,
          codigo_pedido: pedido.codigo,
          importe: pedido.importe_total,
        };
        this.mesasServicio.addMovimientos('mesas', mesa.id, data);
      });

      //subir cada mesa
    }
    if (pedido.estado == 'en preparación') {
      pedido.mesas.forEach(item => item.estado = MesaEstados.esperando);
      //subir cada mesa
    }
    else if (pedido.estado == 'servido') {
      pedido.mesas.forEach(item => item.estado = MesaEstados.comiendo);
    }
    else if (pedido.estado == 'listo para servir') {
      pedido.mesas.forEach(item => item.estado = MesaEstados.esperando);
      pedido.fechaListo = this.ahora;
    }
    else if (pedido.estado == 'finalizado') {
      pedido.fechaListo = this.ahora;
      pedido.mesas.forEach(item => item.estado = MesaEstados.libre);

    }


    this.authservicio.spinner = true;

    let respuesta = this.pedidoServicio.modificar(pedido).then((res) => {
      this.actualizarEstadoMesas(pedido.mesas)
      console.log("Document successfully written!,", pedido.estado);
      this.authservicio.spinner = false;
      this.modificar = -1;
    });


  }

  async actualizarEstadoMesas(mesas: Mesa[]) {
    mesas.forEach(item => this.mesasServicio.modificar(item));

  }


  CancelarPedido() {
    swal2.fire({
      title: 'Confirma cancelar el pedido?',
      text: "Se borrará la solicitud!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.value) {
        this.productosPedidos = [];
        this.costoTotal = 0;
        swal2.fire(
          'Cancelado!',
          'Se eliminó la información del pedido.',
          'success'
        )
      }
    })
  }

  CancelarPedidoRealizado() {
    //this.spinner = true;


    swal2.fire({
      title: 'Confirma cancelar el pedido?',
      text: "Se borrará la solicitud!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.value) {
        this.spinner = true;
        this.productosPedidos = [];
        this.costoTotal = 0;
        this.pedido_iniciado = true;
        this.pedido_completado = false;
        this.pedidoServicio.cambiarEstado(this.pedidoServicio.codigo, EstadoPedido.cancelado).then((res) => {
          console.log("Document successfully written!");
          this.spinner = false;
          swal2.fire(
            'Cancelado!',
            'Se eliminó la información del pedido.',
            'success'
          )

        });

      }
    })
  }


  CancelarPedidoRealizado2(pedido: Pedido) {
    //this.spinner = true;


    swal2.fire({
      title: 'Confirma cancelar el pedido?',
      text: "Se borrará la solicitud!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.value) {
        this.spinner = true;
        //reistro el emplado que lo tomo
        pedido.productos.forEach(producto =>
          producto.estado = EstadoPedido.cancelado
        );
        this.pedido.mesas.forEach(mesa =>
          mesa.estado = MesaEstados.libre)

        pedido.estado = EstadoPedido.cancelado;

        this.pedidoServicio.modificar(pedido).then((res) => {
          this.actualizarEstadoMesas(pedido.mesas);
          console.log("Document successfully written!");
          this.spinner = false;
          swal2.fire(
            'Cancelado!',
            'Se eliminó la información del pedido.',
            'success'
          )

        });

      }
    })
  }


  tomarPedido(pedido: Pedido, tiempo_estimado: number) {
    this.authservicio.spinner = true;
    pedido.estado = EstadoPedido.enpreparacion;
    if (pedido.tiempo_estimado > 0 || tiempo_estimado > 0) {
      //queda el mayor tiempo
      if (pedido.tiempo_estimado < tiempo_estimado) {
        pedido.tiempo_estimado = tiempo_estimado;
        pedido.fechaTomado = this.ahora;
      }
      //reistro el emplado que lo tomo
      pedido.productos.forEach(producto => {
        if (producto.estado == EstadoPedido.pendiente && producto.tipo_empleado == this.perfil) {
          producto.empleado = this.authservicio.usuarioLoguiado;
          producto.estado = EstadoPedido.enpreparacion;
        }

      });

      const data = {
        uid: this.authservicio.usuarioLoguiado.id,
        usuario: this.authservicio.usuarioLoguiado.email,
        nombre: this.authservicio.usuarioLoguiado.nombre,
        perfil: this.authservicio.usuarioLoguiado.perfil,
        codigo: pedido.codigo,
        fecha: this.ahora,
      };

      this.usuarioServicio.addOperaciones('usuarios', this.authservicio.usuarioLoguiado.id, data).then(() => this.ModificarPedido(pedido))

    }
    else {
      this.authservicio.spinner = false;
      swal2.fire(
        'Error!',
        'Para tomar el pedido requiere un tiempo estimado de preparación.',
        'error'
      )

    }
  }




  finalizarPedido(pedido: Pedido) {
    this.authservicio.spinner = true;
    pedido.estado = EstadoPedido.finalizado;

    //reistro el emplado que lo tomo
    pedido.productos.forEach(producto => {
      producto.estado = EstadoPedido.finalizado;

      const data = {
        pedido: pedido.codigo,
        nombre: producto.nombre,
        codigo_porducto: producto.codigo,
        fecha_tomado: pedido.fechaTomado,
        fecha_listo: pedido.fechaListo,
        tiempo_estimado: pedido.tiempo_estimado,
        costo: producto.costo,
        cantidad: producto.cantidad,
        sector: producto.tipo_empleado,
        empleado: producto.empleado.email,
        mozo: pedido.mozo,

      };

      this.pedidoServicio.addProductosVendidos(data);

    }
    );

    this.ModificarPedido(pedido);
  }


  hayProductosPendientes(pedido: Pedido): boolean {
    let retorno: boolean = false;
    pedido.productos.forEach(producto => {
      if (producto.estado == EstadoPedido.pendiente) {
        retorno = true;
      }
    });

    return retorno;
  }

  //chequea si algun producto esta distinto a listo
  pedidoEstaListo(pedido: Pedido): boolean {
    let mapeados = pedido.productos
      .filter(function (e) { return e['estado'] == EstadoPedido.listoaraservir; });

    if (pedido.productos.length == mapeados.length) {
      swal2.fire(
        'listoo!',
        'prductos:' + pedido.productos.length + ' mapeados:' + mapeados.length,
        'success'
      )
      return true;
    }
    else {
      return false;
    }

  }

  //chequea si algun producto esta distinto a servido
  pedidoServido(pedido: Pedido): boolean {
    let mapeados = pedido.productos
      .filter(function (e) { return e['estado'] == EstadoPedido.servido; });
    if (pedido.productos.length == mapeados.length) {
      return true;
    }
    else {
      return false;
    }
  }



  segunEmpleado(estado: any, pedido: Pedido): boolean {
    switch (estado) {
      case EstadoPedido.iniciado:
        //me importa que solo lo vea los mozos
        if (this.authservicio.usuarioLoguiado.perfil != 'Mozo') {
          console.log('hide Iniciado');
          return false;
        }
        break;
      case EstadoPedido.pendiente:
        //me importa si es mozo _> que vea solo sus pedidos

        if (this.authservicio.usuarioLoguiado.perfil == 'Mozo' && pedido.mozo.email != this.authservicio.usuarioLoguiado.email) {
          console.log('hide pendiente no es el mozo del pedido');
          return false;
        }
        //si es si es otro que los vea
        if (this.authservicio.usuarioLoguiado.perfil != 'Mozo' && this.tipoEmpleadoEstado(pedido.productos, estado) == false) {
          console.log('hide pendiente tiene productos para este empleado');
          return false;
        }

        break;
      case EstadoPedido.enpreparacion:
        //enconces me importa que si es mozo _> que vea solo sus pedidos
        if (this.authservicio.usuarioLoguiado.perfil == 'Mozo' && pedido.mozo.email != this.authservicio.usuarioLoguiado.email) {
          console.log('hide preparando no es el mozo del pedido');
          return false;
        }

        if (this.authservicio.usuarioLoguiado.perfil != 'Mozo' && this.tieneProductosEstado(pedido.productos, estado) == false) {
          console.log('hide preparando tiene productos para este empleado');
          return false;
        }
        break;

      //enconces me importa que si es otro _> que vea solo sus pedidos
      case EstadoPedido.listoaraservir:
        if (this.authservicio.usuarioLoguiado.perfil == 'Mozo' && pedido.mozo.email != this.authservicio.usuarioLoguiado.email) {
          console.log('hide listo no es el mozo del pedido');
          return false;
        }

        if (this.authservicio.usuarioLoguiado.perfil != 'Mozo' && this.tieneProductosEstado(pedido.productos, estado) == false) {
          console.log('hide listo tiene productos para este empleado');
          return false;
        }
        break;
      //idem anteriro
      case EstadoPedido.servido:
        //me importa si es mozo _> que vea solo sus pedidos
        if (this.authservicio.usuarioLoguiado.perfil == 'Mozo' && pedido.mozo.email != this.authservicio.usuarioLoguiado.email) {
          console.log('hide servido no es el mozo del pedido');
          return false;
        }
        //si es si es otro que los vea
        if (this.authservicio.usuarioLoguiado.perfil != 'Mozo') {
          console.log('hide servido tiene productos para este empleado');
          return false;
        }
        break;
      case EstadoPedido.finalizado:
        //me importa si es mozo _> que vea solo sus pedidos
        if (this.authservicio.usuarioLoguiado.perfil == 'Mozo' && pedido.mozo.email != this.authservicio.usuarioLoguiado.email) {
          console.log('hide servido no es el mozo del pedido');
          return false;
        }
        //si es si es otro que los vea
        if (this.authservicio.usuarioLoguiado.perfil != 'Mozo') {
          console.log('hide servido tiene productos para este empleado');
          return false;
        }

        break;
      default:
        break;

    }
    return true;
  }


  tipoEmpleadoEstado(list: any[], estado: any): boolean {

    return list.some(producto => producto.estado == estado && producto.tipo_empleado == this.authservicio.usuarioLoguiado.perfil)

  }

  tieneProductosEstado(list: any[], estado: any) {
    return list.some(item => (item.empleado != undefined && item.empleado.email == this.authservicio.usuarioLoguiado.email) && item.estado == estado);

  }

}