import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PedidosEstadosComponent } from './pedidos-estados.component';

describe('PedidosEstadosComponent', () => {
  let component: PedidosEstadosComponent;
  let fixture: ComponentFixture<PedidosEstadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PedidosEstadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PedidosEstadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
