import { Component, OnInit, EventEmitter } from '@angular/core';
import { ProductoService } from 'src/app/servicios/producto.service';
import { Producto } from 'src/app/clases/producto';
import { Router } from '@angular/router';
import { FotoService } from 'src/app/servicios/foto.service';
import { AuthService } from 'src/app/servicios/auth.service';
import { Usuario } from 'src/app/clases/usuario';
import { Pedido } from 'src/app/clases/pedido';
import { PedidoService } from 'src/app/servicios/pedido.service';
import { EstadoPedido } from 'src/app/enumerados/estado-pedido.enum';
import { NgxPrintModule } from 'ngx-print';



import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import swal2 from 'sweetalert2';

import { Routes, ActivatedRoute } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ProductoPedido } from 'src/app/clases/producto-pedido';
import { Mesa } from 'src/app/clases/mesa';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';
import { MesaEstados } from 'src/app/enumerados/mesa-estados.enum';
import { MesaService } from 'src/app/servicios/mesa.service';

@Component({
  selector: 'app-pedido-abm',
  templateUrl: './pedido-abm.component.html',
  styleUrls: ['./pedido-abm.component.css']
})
export class PedidoAbmComponent implements OnInit {

  opts: ISlimScrollOptions;
  scrollEvents: EventEmitter<SlimScrollEvent>;

  productos: Producto[] = [];
  filtro: Producto[] = [];
  usuario: Usuario;
  perfil: string;
  pedido: Pedido;
  productosPedidos: ProductoPedido[] = [];
  mesa: string = "";
  costoTotal: number = 0;
  pedido_iniciado: boolean;
  cliente: Usuario = new Usuario();
  codigo: string;
  categoria: string = "todas";
  mesasElegidas: Mesa[] = [];

  private ok: boolean; //Login OK
  private error: boolean; //Login fallido
  public formularioIniciar: FormGroup;
  private errorDatos: boolean; //Error en el formato de datos de correo o clave
  private enEspera: boolean; //Muestra u oculta el spinner
  pedido_completado: boolean;


  public archivo: File;
  files: File[] = [];
  ahora: Date;
  formulariocodigo: FormGroup;



  constructor(private usuarioServicio: FotoService, private mesasServicio: MesaService ,private router: Router, public formBuilder: FormBuilder, public authservicio: AuthService, private serviciousuarios: FotoService, private authservice: AuthService, private servicioproductos: ProductoService, public pedidoServicio: PedidoService, private route: Router) {
    this.cliente.nombre = "";

    this.formularioIniciar = this.formBuilder.group(
      {
        cliente: ['', Validators.compose([Validators.minLength(2), Validators.required])]
      });

      this.formulariocodigo = this.formBuilder.group(
        {
          email: ['', Validators.compose([Validators.email, Validators.required])]
        });
    this.usuario = this.authservice.usuarioLoguiado;
    this.perfil = this.authservice.usuarioLoguiado.perfil;
    console.log('perfil', this.perfil);
    console.log('codigo', this.pedidoServicio.codigo);
    console.log('archivo', this.archivo);
    if (this.perfil == 'Cliente') {
      this.cliente = this.authservice.usuarioLoguiado
    }
  }

  ngOnInit() {
    this.traerTodasUsuarios();
    this.pedidoServicio.updateTime().then(res => this.pedidoServicio.traerFecha().subscribe((doc) => {
      this.ahora = new Date(doc.data().hoy.toDate());
      console.log(this.ahora);

    }));
    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.opts = {
      position:'right' ,
      barBackground: '#C9C9C9', // 
      barOpacity: '0.8', // 0.8
      barWidth: '10', // 10
      barBorderRadius:'20', // 20
      barMargin:'0', // 0
      gridBackground: '#D9D9D9', // #D9D9D9
      gridOpacity: '1', // 1
      gridWidth: '2', // 2
      gridBorderRadius: '20', // 20
      gridMargin: '0', // 0
      alwaysVisible:true, // true
      visibleTimeout: 1000, // 1000
     
    }
 
   // this.play();
  }
 
  play(): void {
    let event = null;
 
    Promise.resolve()
      .then(() => this.timeout(3000))
      .then(() => {
        event = new SlimScrollEvent({
          type: 'scrollToBottom',
          duration: 2000,
          easing: 'inOutQuad'
        });
 
        this.scrollEvents.emit(event);
      })
      .then(() => this.timeout(3000))
      .then(() => {
        event = new SlimScrollEvent({
          type: 'scrollToTop',
          duration: 3000,
          easing: 'outCubic'
        });
 
        this.scrollEvents.emit(event);
      })
      .then(() => this.timeout(4000))
      .then(() => {
        event = new SlimScrollEvent({
          type: 'scrollToPercent',
          percent: 80,
          duration: 1000,
          easing: 'linear'
        });
 
        this.scrollEvents.emit(event);
      })
      .then(() => this.timeout(2000))
      .then(() => {
        event = new SlimScrollEvent({
          type: 'scrollTo',
          y: 0,
          duration: 4000,
          easing: 'inOutQuint'
        });
 
        this.scrollEvents.emit(event);
      });
  }
 
  timeout(ms: number): Promise<void> {
    return new Promise(resolve => setTimeout(() => resolve(), ms));
  }

  

  onSelect($event) {
    if (this.archivo == undefined) {
      console.log($event);
      this.files.push(...$event.addedFiles);
      this.archivo = this.files[0];
    }

  }

  onRemove($event) {
    console.log($event);
    this.files.splice(this.files.indexOf($event), 1);
    this.archivo = undefined;
  }

  addMesa($event) {
    this.mesasElegidas.push($event);
  }

  restMesa($event) {
    const index = this.mesasElegidas.indexOf($event);
    this.mesasElegidas.splice(index, 1);

  }

  subirFotoPedido() {
    if (this.archivo != undefined) {
      console.log(this.archivo.name, this.archivo, this.codigo)
      this.pedidoServicio.subirFoto(this.archivo.name, this.archivo, this.pedidoServicio.codigo).then(() => {
        swal2.fire({
          title: 'Registro Exitoso!',
          text: 'Se guardo la Foto',
          icon: 'success',
          confirmButtonText: 'ok'
        })
      })
    } else {
      swal2.fire({
        icon: 'error',
        title: 'Imagen no selecionada...',
        text: 'Olvido adjuntar la foto!',

      })
    }

  }

  public getError(form: any, controlName: string): string {
    let error: any;
    let mse = "";
    const control = form.get(controlName);
    if (control.touched && control.errors != null) {
      console.info(JSON.stringify(control.errors));
      error = JSON.parse(JSON.stringify(control.errors));
      if (error.required) {
        mse = "Campo requerido.";
      }
      if (error.minlength != undefined) {
        mse = "Error en logintud mínima requerida.";
      }
      if (error.maxlength != undefined) {
        mse = "Error en la longitud máxima.";
      }

      if (error.pattern != undefined) {
        mse = "Error en el tipo de dato.";
      }
    }
    return mse;
  }




  public getOk(): boolean {
    return this.ok;
  }

  public getErrorDatos(): boolean {
    return this.errorDatos;
  }

  public getEnEspera(): boolean {
    return this.enEspera;
  }

  getUnique(arr, comp) {
    var unique = arr
      .map(function (e) { return e[comp]; })
      // store the keys of the unique objects
      .map(function (e, i, final) { return final.indexOf(e) === i && i; })
      // eliminate the dead keys & store unique objects
      .filter(function (e) { return arr[e]; }).map(function (e) { return arr[e]; });
    return unique;
  }



  traerTodasUsuarios() {
    this.servicioproductos.traerTodos('productos')
      .subscribe(productos => {
        this.productos = productos.filter(p=> p.baja != true);
        this.filtro = this.getUnique(this.productos, 'sucursal');
        console.info(this.filtro);
      });
  }


  prueba() {
    this.servicioproductos.prueba();
  }


  mostrar(producto: any) {
    //this.paisesService.verDetalle(pais).subscribe(resp => resp);
    this.servicioproductos.verDetalle(producto).subscribe(resp => resp);
  }

  agregar(producto: any) {
    //this.paisesService.verDetalle(pais).subscribe(resp => resp);
    if (producto.cantidad > 0) {
      let productoPedido = new ProductoPedido();
      productoPedido.URL = producto.URL;
      productoPedido.categoria = producto.categoria;
      productoPedido.codigo = producto.codigo;
      productoPedido.costo = producto.costo;
      productoPedido.descripcion = producto.descripcion;
      productoPedido.nombre = producto.nombre;
      productoPedido.tipo_empleado = producto.tipo_empleado;
      productoPedido.cantidad = producto.cantidad;
      productoPedido.monto = (productoPedido.costo * productoPedido.cantidad)
      
      if(this.authservice.usuarioLoguiado.perfil == 'Cliente'){
        productoPedido.estado = EstadoPedido.iniciado;
      }

      this.productosPedidos.push(productoPedido);
      this.costoTotal = this.costoTotal + productoPedido.monto;
    }
    else {
      //ingrese cantidad

    }
  }


  quitarProducto(producto: any) {
    const index = this.productosPedidos.indexOf(producto);
    this.productosPedidos.splice(index, 1);
    this.costoTotal = this.costoTotal - (producto.costo * producto.cantidad);

  }


  IniciarPedido() {

    if (this.formularioIniciar.valid) {
      if(this.mesasElegidas.length > 0){
        this.pedido = new Pedido();
        this.pedido.cliente = this.cliente;
        this.pedido.mesas = this.mesasElegidas//ocupar mesa       
        this.pedido.mozo = this.usuario;//mozo       
        this.pedido.estado = EstadoPedido.iniciado;
        this.pedido_iniciado = true;
        this.pedido_completado = false;
        //nombre cliente   
      } else{
        swal2.fire(
          'Elija una Mesa de la lista',
          'Debe seleccionar por lo menos una mesa',
          'warning'
        )
      }
      }
     
  }


  RealizarPedido() {


    if (this.pedido != undefined && this.pedido.estado == "iniciado" && this.productosPedidos.length > 0) {
       this.pedido.mesas.forEach(item => item.estado = MesaEstados.ocupada);
      if (this.authservice.usuarioLoguiado.perfil == 'Cliente') {
        this.pedido.productos = this.productosPedidos;
        this.pedido.importe_total = this.costoTotal;
        this.pedido.fecha = this.ahora;
      } else {
        this.pedido.productos = this.productosPedidos;
        this.pedido.estado = EstadoPedido.pendiente;
        this.pedido.importe_total = this.costoTotal;
        this.pedido.fecha = this.ahora;

      }

      let respuesta = this.pedidoServicio.subir(this.pedido).then((res) => {
        console.log("Document successfully written!", res);
        this.codigo = this.pedidoServicio.codigo;
        this.actualizarEstadoMesas(this.pedido.mesas);

        const data = {
          uid: this.authservicio.usuarioLoguiado.id,
          usuario: this.authservicio.usuarioLoguiado.email,
          nombre: this.authservicio.usuarioLoguiado.nombre,
          perfil: this.authservicio.usuarioLoguiado.perfil,         
          codigo: this.pedido.codigo, 
          fecha: this.ahora,    
      };
  
      this.usuarioServicio.addOperaciones('usuarios', this.authservicio.usuarioLoguiado.id, data)
      this.pedido.mesas.forEach(mesa => {
        const dataMesa = {
          numero: mesa.numero,
          id_mesa: mesa.id,
          fecha: this.pedido.fecha,
          codigo_pedido: this.pedido.codigo,
          importe: this.pedido.importe_total,
        };
        //this.mesasServicio.addMovimientos('mesas', mesa.id, dataMesa);
        this.mesasServicio.addMovimientosMesas(dataMesa).then(()=> console.log('succes'));
      });

      });
      this.pedido_completado = true;
    }
    else {
      console.log("");
    }
  }

  
actualizarEstadoMesas(mesas: Mesa[]){
  mesas.forEach(item => {if(item.estado == 'libre'){item.habilitada = true} this.mesasServicio.modificar(item)} );

}

  CancelarPedido() {
    swal2.fire({
      title: 'Confirma cancelar el pedido?',
      text: "Se borrará la solicitud!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.value) {
        this.productosPedidos = [];
        this.costoTotal = 0;
        swal2.fire(
          'Cancelado!',
          'Se eliminó la información del pedido.',
          'success'
        )
      }
    })
  }

  CancelarPedidoRealizado() {
    //this.spinner = true;


    swal2.fire({
      title: 'Confirma cancelar el pedido?',
      text: "Se borrará la solicitud!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.value) {
        this.pedidoServicio.spinner = true;
        this.productosPedidos = [];
        this.costoTotal = 0;
        this.pedido_iniciado = true;
        this.pedido_completado = false;
        this.pedidoServicio.cambiarEstado(this.pedidoServicio.codigo, EstadoPedido.cancelado).then((res) => {
          console.log("Document successfully written!");
          this.pedidoServicio.spinner = false;
          swal2.fire(
            'Cancelado!',
            'Se eliminó la información del pedido.',
            'success'
          )

        });

      }
    })
  }

  EnviarCodigo(codigo, correo){
    const data={
      correo,
      codigo,
    }
    this.pedidoServicio.addCodigoEmail(data).then(()=> {
      swal2.fire(
      'Enviado!',
      'Se envió la información del pedido.',
      'success'
    )
  }
  ); 
  }

}