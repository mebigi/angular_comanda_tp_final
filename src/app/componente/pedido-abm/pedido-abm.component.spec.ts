import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PedidoAbmComponent } from './pedido-abm.component';

describe('PedidoAbmComponent', () => {
  let component: PedidoAbmComponent;
  let fixture: ComponentFixture<PedidoAbmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PedidoAbmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PedidoAbmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
