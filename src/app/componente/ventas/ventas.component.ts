import { Component, OnInit } from '@angular/core';
import { FotoService } from 'src/app/servicios/foto.service';
import { DatePipe } from '@angular/common';
import { PedidoService } from 'src/app/servicios/pedido.service';
import * as jsonexport from "jsonexport/dist"
import { saveAs } from 'file-saver';
import { RangoFechaPipe } from 'src/app/pipes/rango-fecha.pipe';
import { ChartType } from 'chart.js';
import { MultiDataSet, Label } from 'ng2-charts';



@Component({
  selector: 'app-ventas',
  templateUrl: './ventas.component.html',
  styleUrls: ['./ventas.component.css']
})
export class VentasComponent implements OnInit {
  doughnutChartLabels: Label[] = [];
  doughnutChartData: MultiDataSet = [[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]];
  doughnutChartType: ChartType = 'doughnut';

  ventas: any[] = [];
  Fechavi: any[] = [];
  Fechavf: any[] = [];
  ventasMozos: any[] = [];
  ventasBartenders: any[] = [];
  ventasCocineros: any[] = [];
  bartenders: any[] = [];
  cocineros: any[] = [];
  filtro: any;
  arCsv: any[] = [];

  bartenderCantidad: { dia: string, empleado: string, cantidad: number }[] = [];
  cocineroCantidad: { dia: string, empleado: string, cantidad: number }[] = [];
  mozos: any[] = [];
  mozosVentas: any[] = [];
  ventasFulano: any[] = [];
  fulano: string;
  diaf: any;
  dia: any;
  max: any;
  min: any;

  productoCantidad: any[] = [];
  productos: any[] = [];
  ventasFitradas: any[] = [];
  noatiempo:any[] = [];
  cancelados: any[];

  constructor(private pedidoServicio: PedidoService) { }

  ngOnInit() {

    //      15
    let pipe2: DatePipe = new DatePipe('en-US');
    this.pedidoServicio.traerTodosVendidos().subscribe(ventas => {
      this.ventas = ventas;

      this.ventas.forEach(venta => { venta['dia'] = pipe2.transform(venta.fecha_listo, 'MM/dd/yyyy'); venta['diaf'] = pipe2.transform(venta.fecha_listo, 'MM/dd/yyyy'); })
      this.Fechavi = this.getUnique(this.ventas, 'dia');
      this.Fechavf = this.getUnique(this.ventas, 'dia');
      this.diaf = this.Fechavi[0].dia;
      this.dia = this.Fechavi[(this.Fechavi.length-1)].dia;

     // this.ventasFitradas = this.ventas.filter(venta => new Date(venta.fecha_listo).getTime() >= new Date(this.dia).getTime() && new Date(venta.fecha_listo).getTime() <= new Date(this.diaf).getTime())

     this.ventasFitradas = this.ventas;

      if (this.ventasFitradas.length > 0) {
        this.productos = this.getUnique(this.ventasFitradas, 'codigo_porducto');
        console.log("entro?",this.ventas, this.productos,  this.ventasFitradas);

       this.noatiempo = this.ventasFitradas.map(this.mapProductoCantidad).filter(p=> p.atiempo == false);
       console.log("this.noatiempo", this.noatiempo);

        this.productos.forEach(producto => {
          let p = {};
          p['cantidad'] =
            this.cantidad(producto.codigo_porducto, this.ventasFitradas);
          p['nombre'] =
           producto.nombre;
          p['codigo'] =
           producto.codigo_porducto;
          
           this.productoCantidad.push(p);
        }
        );

        console.log("atiempo false",this.productoCantidad.filter(p=> p.atiempo == false));

        this.max = this.productoCantidad.reduce((maximo, actual) => {
          if (actual.cantidad > maximo.cantidad) {
            maximo = actual;
          }
          return maximo
        });

        console.log("max?", this.max);         
        this.min = this.productoCantidad.reduce((maximo, actual) => {
          if (actual.cantidad < maximo.cantidad) {
            maximo = actual;
          }
          return maximo
        });
  
        console.log("min?", this.min); 
      };               

      //this.productoCantidad = this.ventas.map(this.mapProductoCantidad);
      console.log(this.ventas);
      console.log("this.productos", this.productos);
      console.log("this.productoCantidad", this.productoCantidad);



      this.doughnutChartLabels= this.productoCantidad.map(p => p.nombre)
      this.doughnutChartData = this.productoCantidad.map(p => p.cantidad)

    });


    this.pedidoServicio.traerTodos('pedidos')
      .subscribe(pedidos => {
      this.cancelados = pedidos.filter(p=> p.estado == 'cancelado');        
      console.log("this.cancelados", this.cancelados);

      });
  


  }


  getUnique(arr, comp) {
    var unique = arr
      .map(function (e) { return e[comp]; })
      // store the keys of the unique objects
      .map(function (e, i, final) { return final.indexOf(e) === i && i; })
      // eliminate the dead keys & store unique objects
      .filter(function (e) { return arr[e]; }).map(function (e) { return arr[e]; });
    return unique;
  }


  exportCSV(myData: any[], dato: any) {
    //let myData= JSON.stringify(Data);   

    let filtado = new RangoFechaPipe();
    // filtado.transform(this.ventas, dato);

    jsonexport(filtado.transform(this.ventas, dato), function (err, csv) {
      if (err) return console.log(err);
      console.log(csv);

      let file = new Blob([csv], { type: 'text/csv;charset=utf-8' });
      saveAs(file, 'operaciones.csv')

    });
  }

  cargarOperacionesEmpleado(email: string) {
    this.fulano = email;
    this.ventasFulano = this.ventas.filter(venta => venta.empleado == email);

  }



  cantidad(producto: any, ventas: any[]): any {
    let cantidad = 0;
    ventas.forEach(item => {
      if (producto == item.codigo_porducto) {
        cantidad = cantidad + item.cantidad;
      }

    }
    );

    console.log(producto, ' ', cantidad);
    return cantidad;
  }


  cantidadMesa(mesa: any, pedidos: any[]): any {
    let cantidad = 0;
    pedidos.forEach(item => {
      if (mesa == item.mesa[0].numero) {
        cantidad = cantidad + 1;
      }

    }
    );
    console.log(mesa, ' ', cantidad);
    return cantidad;
  }



  mapProductoCantidad(venta: any): any {
    let b = {};
    let pipe2 = new DatePipe('en-US')
    b['dia'] = pipe2.transform(venta.fecha_listo, 'MM/dd/yyyy');
    b['diaf'] = pipe2.transform(venta.fecha_listo, 'MM/dd/yyyy');
    b['tomado'] = venta.fecha_tomado;
    b['listo'] = venta.fecha_listo;
    b['estimado'] = venta.tiempo_estimado;
    b['producto'] = venta.codigo_porducto;
    b['nombre'] = venta.nombre;
    b['cantidad'] = venta.cantidad;
    let diff: number = ((new Date(venta.fecha_listo).getTime()) - (new Date(venta.fecha_tomado).getTime()))
    if (diff != 0) {
      diff = diff / 1000;
      diff /= 60;
      console.log(diff);
    }
    if (diff != NaN && diff != undefined && diff <= venta.tiempo_estimado) {
      b['atiempo'] = true;
    }
    else {
      b['atiempo'] = false;
    }

    console.log('b', b);
    return b;

  }


  MaxReducer(reading) {
    return Math.max(reading);
  }

  MinReducer(reading) {
    return Math.min(reading);
  }


}
