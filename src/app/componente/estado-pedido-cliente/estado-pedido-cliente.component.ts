import { Component, OnInit, Output } from '@angular/core';
import { PedidoService } from 'src/app/servicios/pedido.service';
import { AuthService } from 'src/app/servicios/auth.service';
import { Pedido } from 'src/app/clases/pedido';
import { EstadoPedido } from 'src/app/enumerados/estado-pedido.enum';


@Component({
  selector: 'app-estado-pedido-cliente',
  templateUrl: './estado-pedido-cliente.component.html',
  styleUrls: ['./estado-pedido-cliente.component.css']
})
export class EstadoPedidoClienteComponent implements OnInit {
  @Output() pedido: Pedido;
  ahora: Date;
  falta: number = 0;
  servido: boolean;
  listo: boolean;
  enpreparacion: boolean;
  pendiente: boolean;
  iniciado: boolean;
  estado: string = " ";
  codigo: string = " ";


  constructor(public authservicio: AuthService, public pedidoServicio: PedidoService) {

  }

  ngOnInit() {
    this.pedido = new Pedido;
    console.log("this.codigo", this.codigo);
    if (this.pedidoServicio.ultimoPedido != undefined) {
      console.log("this.codigo 1");
      this.authservicio.spinner = true;
      this.pedidoServicio.updateTime().then(res => this.pedidoServicio.traerFecha().subscribe((doc) => {
        this.ahora = new Date(doc.data().hoy.toDate());
        this.estado = this.pedidoServicio.ultimoPedido.estado;
        this.pedido = this.pedidoServicio.ultimoPedido;
        console.log("this.estado", this.estado);
        this.barraEstado();
        this.authservicio.spinner = false;
        this.falta = this.diff_minutes(new Date(this.pedido.fechaTomado), this.ahora, this.pedido.tiempo_estimado);
      }));

    }
    else if (this.pedidoServicio.codigoEntregado != undefined && this.pedidoServicio.codigoEntregado != " ") {
      this.codigo = this.pedidoServicio.codigoEntregado;
      console.log("this.codigo 2");
      this.authservicio.spinner=true;
      this.pedidoServicio.updateTime().then(res => this.pedidoServicio.traerFecha().subscribe((doc) => {
        this.ahora = new Date(doc.data().hoy.toDate()); 
        this.pedidoServicio.buscarUId(this.codigo).subscribe((res) => {
        this.pedido = <Pedido>res.data();      
        console.log( "this.estado",   this.pedido.estado);
        this.estado=this.pedido.estado;
          this.barraEstado();
          this.authservicio.spinner=false;
          this.falta = this.diff_minutes(new Date(this.pedido.fechaTomado), this.ahora, this.pedido.tiempo_estimado);
    
          
        })
      }));     
    }
  }

  diff_minutes(dt2: Date, dt1: Date, t: number) {
    t = t*1000*60;
    var diff = ((t + dt2.getTime()) - (dt1.getTime())) / 1000;
    diff /= 60;
    console.log('var diff', (t + dt2.getTime()), '-', (dt1.getTime()));
    return Math.round(diff);



  }




  estAnim(elementId, animClasses) {
    document.getElementById('pendiente').classList.add(animClasses);
  }


  barraEstado() {
    switch (this.pedido.estado) {
      case EstadoPedido.finalizado:
      case EstadoPedido.servido:
        this.servido = true;
      case EstadoPedido.listoaraservir:
        this.listo = true;
      case EstadoPedido.enpreparacion:
        this.enpreparacion = true;
      case EstadoPedido.pendiente:
        this.pendiente = true;
      case EstadoPedido.iniciado:
        this.iniciado = true;


        break;
      default:
        break;

    }
  }



}
