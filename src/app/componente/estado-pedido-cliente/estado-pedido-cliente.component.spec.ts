import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadoPedidoClienteComponent } from './estado-pedido-cliente.component';

describe('EstadoPedidoClienteComponent', () => {
  let component: EstadoPedidoClienteComponent;
  let fixture: ComponentFixture<EstadoPedidoClienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstadoPedidoClienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadoPedidoClienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
